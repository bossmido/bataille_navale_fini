#ifndef DessinGUI_h
#define DessinGUI_h

//#include <QtCore>
// #include <QtGui/QWidget>
// #include <QtGui/QApplication>
// //#include <Qt/QtOpenGL>
#include "win.h"
#include "event.h"

//#include "Fenetre.h"
#include "Dessinnable.h"

//salut Guy

class DessinGUI : public Dessinnable{
    // DessinGUI(int largeur, int hauteur){
    //     this->config = new ConfigDessin(largeur,hauteur);
    // }

    virtual void initialiser(Option& o){
 // Création d'une fenêtre
      Win wind("Xdatte",640,480,"#ffffff");

  // Dessine une ligne rouge
      wind.DrawLine(10, 10, 300, 80, "#ff0000");
  // Dessine un rectangle plein vert
      wind.FillRectangle(60, 60, 100, 100, "#00ff00");
  // Dessine un rectangle vide bleu
      wind.DrawRectangle(110, 110, 100, 100, "#0000ff");
  // Dessine un cercle plein jaune
      wind.FillCircle(220, 110, 40, 40, "#ffff00");
  // Dessine un cercle vide rouge
      wind.DrawCircle(270, 110, 40, 40, "#ff0000");

  // Dessine trois chaine de caractères
      wind.DrawText(10, 240, "Small", "#ffff00", FONT_SMALL);
      wind.DrawText(110, 240, "Medium", "#ff00ff", FONT_MEDIUM);
      wind.DrawText(210, 240, "Large", "#00ffff", FONT_LARGE);


  // Affiche la fenêtre
      wind.DrawWindow();

      while (1)
      {      
          Event e;
          e = wind.getEvent();
          if (e.getType() == MB1P)
          {
              wind.FillRectangle(e.getX(), e.getY(), 5, 5, "#ff0000");
          }
          wind.DrawWindow();
          wind.delay(50000);
      }
      return;
  }
  virtual int dessinerMenuPrincipal(Menu& m){

  }
  virtual void dessinerMenuChoixNombreJoueur(Menu& m){}
  virtual void dessinerMenuChoixModeJeu(Menu& m){}
  virtual void dessinerBateau(Coordonnee c,Bateau& b,Grille& g){}
  virtual void dessinerListeBateau(Coordonnee c,std::vector<Bateau>* v){}
  virtual void dessinerCase(Coordonnee c,Case& cases,Grille& g){}
  virtual void dessinerCurseur(Coordonnee c,const  Grille& g){}
  virtual void dessinerGrille(Coordonnee c,Grille& g){}
  virtual void dessinerGrilleJoueur(Coordonnee c,Grille& g){}
  virtual void dessinerGrilleAdversaire(Coordonnee c,Grille& g){}
  virtual void dessinerTexteTour(Coordonnee c,const std::string&  tour){}
  virtual void dessinerTexteGrilleAdverse(Coordonnee c,const std::string& tour){}
  virtual void dessinerTexteEtat(Coordonnee c,const std::string& tour){}
  virtual void dessinerTexteDebug(Coordonnee c,const std::string& tour){}
  virtual std::string getEntre(){}
  virtual void desinitialiser(){}
  virtual Touche captureTouche(){}
  virtual void rafraichir(){}
};

#endif