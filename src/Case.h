#ifndef Case_h
#define Case_h

#include <vector>

#include "Dessin.h"
#include "Coordonnee.h"
#include "etat.h"

class Grille;

class Case : public Dessin {

public:

    Case();
    Case(Etatgrille e);
    //Constructeur de copie
    Case(const Case& c);

private:
    Etatgrille occupe;
    Coordonnee cordonnee_parametre;

public:
    Coordonnee GetCoordonnee()const ;
    void setCoordonnee(Coordonnee arg);
    Etatgrille getEtat() const;
    void setEtat(Etatgrille b);
    Case& operator=(const Case& copy);
};

#endif // Case_h
