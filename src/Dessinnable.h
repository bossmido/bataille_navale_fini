//Classe définie pour le pattern adaptateur sinon il faudrait redéfinir les méthodes de dessin dans chaque  
#ifndef Dessinable_h
#define Dessinable_h
#include <string>
#include <vector>

#include "Coordonnee.h"
#include "Option.h"
#include "Option.h"
#include "Grille.h"
#include "Case.h"
#include "Bateau.h"
#include "etat.h"
#include "ConfigDessin.h"
#include "utilitaire.h"
class Menu;
#include "Menu.h"


class Dessinnable{
    protected : 
    ConfigDessin* config;
    public :
    virtual void initialiser(Option& o) = 0;
    virtual int dessinerMenuPrincipal(Menu& m) =0;
    virtual void dessinerMenuChoixNombreJoueur(Menu& m) = 0;
    virtual void dessinerMenuChoixModeJeu(Menu& m) =0;
    virtual void dessinerBateau(Coordonnee c,Bateau& b,Grille& g) = 0;
    virtual void dessinerListeBateau(Coordonnee c,std::vector<Bateau>* v) = 0;
    virtual void dessinerCase(Coordonnee c,Case& cases,Grille& g) = 0;
    virtual void dessinerCurseur(Coordonnee c,const  Grille& g) = 0;
    virtual void dessinerGrille(Coordonnee c,Grille& g) = 0;
    virtual void dessinerGrilleJoueur(Coordonnee c,Grille& g) = 0;
    virtual void dessinerGrilleAdversaire(Coordonnee c,Grille& g) = 0;
    virtual void dessinerTexteTour(Coordonnee c,const std::string&  tour) = 0;
    virtual void dessinerTexteGrilleAdverse(Coordonnee c,const std::string& tour) = 0;
    virtual void dessinerTexteEtat(Coordonnee c,const std::string& tour) = 0;
    virtual void dessinerTexteDebug(Coordonnee c,const std::string& tour) = 0;
    virtual std::string getEntre() = 0;
    virtual void desinitialiser() = 0;
    virtual Touche captureTouche() = 0;
    virtual void rafraichir() = 0;
    //virtual ~Dessinnable();

};
//
#endif