#include "Couleur.h"

void CouleurIO::Write(string texte, string texteCouleur, string FondCouleur)
{
    int texteCouleurNum = 0;
    int FondCouleurNum = 0;

    if(texteCouleur == "noir")
    {
        texteCouleurNum = 30;
    }
    else if(texteCouleur == "rouge")
    {
        texteCouleurNum = 31;
    }
    else if(texteCouleur == "vert")
    {
        texteCouleurNum = 32;
    }
    else if(texteCouleur == "jaune")
    {
        texteCouleurNum = 33;
    }
    else if(texteCouleur == "bleu")
    {
        texteCouleurNum = 34;
    }
    else if(texteCouleur == "magenta")
    {
        texteCouleurNum = 35;
    }
    else if(texteCouleur == "cyan")
    {
        texteCouleurNum = 36;
    }
    else if(texteCouleur == "blanc")
    {
        texteCouleurNum = 37;
    }

    if(FondCouleur == "noir")
    {
        FondCouleurNum = 40;
    }
    else if(FondCouleur == "rouge")
    {
        FondCouleurNum = 41;
    }
    else if(FondCouleur == "vert")
    {
        FondCouleurNum = 42;
    }
    else if(FondCouleur == "jaune")
    {
        FondCouleurNum = 43;
    }
    else if(FondCouleur == "bleu")
    {
        FondCouleurNum = 44;
    }
    else if(FondCouleur == "magenta")
    {
        FondCouleurNum = 45;
    }
    else if(FondCouleur == "cyan")
    {
        FondCouleurNum = 46;
    }
    else if(FondCouleur == "blanc")
    {
        FondCouleurNum = 47;
    }


    cout << "\033[0;" << texteCouleurNum << "m";//Change texte Couleur
    cout << "\033[1;" << FondCouleurNum << "m";//Change Fond Couleur
    cout << texte;

    cout << "\033[0m";//réinitialise la couleur du terminal
}

void CouleurIO::WriteLine(string texte, string texteCouleur, string FondCouleur)
{
    int texteCouleurNum = 0;
    int FondCouleurNum = 0;

    if(texteCouleur == "noir")
    {
        texteCouleurNum = 30;
    }
    else if(texteCouleur == "rouge")
    {
        texteCouleurNum = 31;
    }
    else if(texteCouleur == "vert")
    {
        texteCouleurNum = 32;
    }
    else if(texteCouleur == "jaune")
    {
        texteCouleurNum = 33;
    }
    else if(texteCouleur == "bleu")
    {
        texteCouleurNum = 34;
    }
    else if(texteCouleur == "magenta")
    {
        texteCouleurNum = 35;
    }
    else if(texteCouleur == "cyan")
    {
        texteCouleurNum = 36;
    }
    else if(texteCouleur == "blanc")
    {
        texteCouleurNum = 37;
    }

    if(FondCouleur == "noir")
    {
        FondCouleurNum = 40;
    }
    else if(FondCouleur == "rouge")
    {
        FondCouleurNum = 41;
    }
    else if(FondCouleur == "vert")
    {
        FondCouleurNum = 42;
    }
    else if(FondCouleur == "jaune")
    {
        FondCouleurNum = 43;
    }
    else if(FondCouleur == "bleu")
    {
        FondCouleurNum = 44;
    }
    else if(FondCouleur == "magenta")
    {
        FondCouleurNum = 45;
    }
    else if(FondCouleur == "cyan")
    {
        FondCouleurNum = 46;
    }
    else if(FondCouleur == "blanc")
    {
        FondCouleurNum = 47;
    }


    cout << "\033[0;" << texteCouleurNum << "m";//Change texte Couleur
    cout << "\033[1;" << FondCouleurNum << "m";//Change Fond Couleur
    cout << texte << endl;

    cout << "\033[0m";//Changes Terminal To Default Couleur(texte and Fond)
}

void ConsoleCouleur::texteCouleur(string Couleur)
{
    string texteCouleur = Couleur;
    int texteCouleurNum = 0;

    if(texteCouleur == "noir")
    {
        texteCouleurNum = 30;
    }
    else if(texteCouleur == "rouge")
    {
        texteCouleurNum = 31;
    }
    else if(texteCouleur == "vert")
    {
        texteCouleurNum = 32;
    }
    else if(texteCouleur == "jaune")
    {
        texteCouleurNum = 33;
    }
    else if(texteCouleur == "bleu")
    {
        texteCouleurNum = 34;
    }
    else if(texteCouleur == "magenta")
    {
        texteCouleurNum = 35;
    }
    else if(texteCouleur == "cyan")
    {
        texteCouleurNum = 36;
    }
    else if(texteCouleur == "blanc")
    {
        texteCouleurNum = 37;
    }
    cout << "\033[0;" << texteCouleurNum << "m";//Change texte Couleur
}

void ConsoleCouleur::FondCouleur(string Couleur)
{
    string FondCouleur = Couleur;
    int FondCouleurNum = 0;

    if(FondCouleur == "noir")
    {
        FondCouleurNum = 40;
    }
    else if(FondCouleur == "rouge")
    {
        FondCouleurNum = 41;
    }
    else if(FondCouleur == "vert")
    {
        FondCouleurNum = 42;
    }
    else if(FondCouleur == "jaune")
    {
        FondCouleurNum = 43;
    }
    else if(FondCouleur == "bleu")
    {
        FondCouleurNum = 44;
    }
    else if(FondCouleur == "magenta")
    {
        FondCouleurNum = 45;
    }
    else if(FondCouleur == "cyan")
    {
        FondCouleurNum = 46;
    }
    else if(FondCouleur == "blanc")
    {
        FondCouleurNum = 47;
    }
    cout << "\033[1;" << FondCouleurNum << "m";//Change Fond Couleur
}

void ConsoleCouleur::Reset()
{
    cout << "\033[0m";//Changes Terminal To Default Couleur(texte and Fond)
}

