#ifndef JoueurIA_h
#define JoueurIA_h
class Grille;
#include "Joueur.h"

class GrillePersonnel;
class Joueur;

class JoueurIA : public Joueur
{
public:
    JoueurIA();
    JoueurIA(std::string nom,int largeur,int hauteur,int id) :  Joueur(nom,largeur,hauteur,id){}
    JoueurIA(std::string nom,int largeur,int hauteur,std::vector<Bateau> v,int id) : Joueur(nom,largeur,hauteur,v,id){}
    //et appelé au lieu de traiteraction dans Jeu (je sais c'est mal fout mal foutu)
    void jouer( std::string& chaine, Joueur& j, Joueur& adversaire,const Option& o);
    ~JoueurIA();

};

#endif