
#include <iostream>
#include <string>

using namespace std;

class CouleurIO
{
    public:
    void Write(string texte, string texteCouleur, string FondCouleur);
    void WriteLine(string texte, string texteCouleur, string FondCouleur);
};

class ConsoleCouleur
{
    public:
    void texteCouleur(string Couleur);
    void FondCouleur(string Couleur);
    void Reset();
};
