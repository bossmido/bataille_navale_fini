#include "Jeu.h"

Jeu::Jeu(const Option& o){
  srand (time (0));
  Joueur* nouveau_joueur;
  if(o.nb_joueur != -1){
    std::stringstream s;
    std::string nom_joueur;
    
    std::vector<Joueur*> v;
    this->liste_joueurs = v ;
     #ifndef DEBUG
    if(o.IA){
        nom_joueur = "joueur humain";
        nouveau_joueur =new Joueur(nom_joueur,o.largeur,o.hauteur,1);
        this->liste_joueurs.push_back(nouveau_joueur);
         nom_joueur = "joueur IA";
          nouveau_joueur =new JoueurIA(nom_joueur,o.largeur,o.hauteur,2);
        this->liste_joueurs.push_back(nouveau_joueur);
    }else
    if(o.rapide){
      for (int i = 1; i < o.nb_joueur+1; i++)
      {
        s.clear();
        s<<"Joueur "<<i;
        nom_joueur = s.str();
        nouveau_joueur =new Joueur(nom_joueur,o.largeur,o.hauteur,i);
        this->liste_joueurs.push_back(nouveau_joueur);
        if(o.hasard){
          std::vector<Bateau>* tmp_boucle =  nouveau_joueur->getListeBateau();
          for (std::vector<Bateau>::iterator i = tmp_boucle->begin(); i != tmp_boucle->end(); i++)
          {
            (*i).trouvePlace((Grille &)nouveau_joueur->GetGrillePersonnel());
          }
        }
      }
      if(o.nb_joueur == 2){
        this->jouer_une_partie_a_deux_joueurs(o);
      }
    }
      #endif

  }
    #ifdef DEBUG
  std::vector<Bateau> liste_bateaux_test;
  Coordonnee* c=  new Coordonnee(3,8);
  Bateau* b =new Bateau("Porte_avion", 5,*c,DROITE);
  liste_bateaux_test.push_back(*b);
  b= new Bateau("fregate",2,Coordonnee(3,5),HAUT);
  liste_bateaux_test.push_back(*b);
  b = new Bateau("fregate", 2,Coordonnee(1,1),HAUT);
  liste_bateaux_test.push_back(*b);

  b =new Bateau("couillon sur la mer", 2,Coordonnee(3,5),DROITE);
  liste_bateaux_test.push_back(*b);
  c = new Coordonnee(4,4);
  b=new  Bateau("sous_marin", 4,*c,DROITE);
  liste_bateaux_test.push_back(*b);


  nouveau_joueur =new Joueur("Joueur 1",o.largeur,o.hauteur,liste_bateaux_test);
  this->liste_joueurs.push_back(*nouveau_joueur);
  this->JouerModeTest();
    #endif
  #ifdef DEBUG
  std::cerr<<"le jeu est fini laul"<<std::endl;
   #endif
}

void Jeu::JouerModeTest(const Option& o){
  this->liste_joueurs[0]->ajouterAdversaire(this->liste_joueurs[0]);
  std::string nom_tour;
  bool joueur_a_jouer = false;
  bool premier_tour = false;
  int code_touche = 0;
  std::string commande;
  int i = 0;
  while(!joueur_a_jouer){
    if(liste_joueurs.size() == 0){

     canvas->dessinerTexteEtat(Coordonnee(), "pas de joueur");
   }else{
    for (std::vector<Joueur*>::iterator i = this->liste_joueurs.begin(); i != this->liste_joueurs.end(); i++)
    {
      sleep(1);
      nom_tour=  "tour : "+(*i)->getNom();
      canvas->dessinerTexteEtat(Coordonnee(),  nom_tour);
      canvas->rafraichir();
      this->liste_joueurs[1]->getListeBateau();
            if(!(*i)->estvalider()){//teste si le joueur  a au moins placé ses bateaux
              for (int j = 0; j < 5;j++)
                { (*i)->dessiner(Coordonnee(1,1));
                 commande =  canvas->getEntre();
                 this->traiterAction(commande,*(*i),*(*i)->getAdversaires()[0],o);
                     //canvas->dessinerTexteEtat(Coordonnee(0,0),"placement des bateaux");
                 refresh(); 

               }
               (*i)->setvalide(true);
             }
             (*i)->dessiner(Coordonnee(0,0));
             commande =  canvas->getEntre();
             this->traiterAction(commande,*(*i),*(*i)->getAdversaires()[0],o);
           }
         }
         canvas->rafraichir();
         sleep(2);
         canvas->dessinerTexteEtat(Coordonnee(), "fin du tour");
         canvas->rafraichir();

       }

    //     while((code_touche = getch()) != KEY_F(12))
    //     {   switch(code_touche)
    //         {   case KEY_DOWN:
    //             break;
    //             case KEY_UP:
    //             break;
    //             case KEY_LEFT :
    //             break;
    //             case KEY_RIGHT :
    //             break;

    //     }

    // }

     }  


     void Jeu::jouer_une_partie_a_deux_joueurs(const Option& o)
     {
      this->liste_joueurs[0]->ajouterAdversaire(this->liste_joueurs[1]);
      this->liste_joueurs[1]->ajouterAdversaire(this->liste_joueurs[0]);
      std::string nom_tour;
      bool joueur_a_jouer = false;
      bool premier_tour = false;
      int code_touche = 0;
      std::string commande;
      int i = 0;
      int nombre_tour =0;
      while(!joueur_a_jouer){
        if(liste_joueurs.size() == 0){

         canvas->dessinerTexteEtat(Coordonnee(), "pas de joueur");
       }else{
        for (std::vector<Joueur*>::iterator i = this->liste_joueurs.begin(); i != this->liste_joueurs.end(); i++)
        {
          sleep(1);
          nom_tour=  "tour : "+(*i)->getNom();
          canvas->dessinerTexteEtat(Coordonnee(),  nom_tour);
          canvas->rafraichir();
          sleep(1);

            // if(!(*i)->estvalider()){//teste si le joueur  a au moins placé ses bateaux
            //   for (int j = 0; j < 5;j++)
            //     { (*i)->dessiner(Coordonnee(1,1));
            //      commande =  canvas->getEntre();
            //      this->traiterAction(commande,*(*i),*(*i)->getAdversaires()[0]);
            //          canvas->dessinerTexteEtat(Coordonnee(0,0),"placement des bateaux");
            //      refresh(); 

            //    }
            //    (*i)->setvalide(true);
            //  }
          (*i)->dessiner(Coordonnee(0,0));
          commande =  canvas->getEntre();
          if((*i)->getid()==1){
            nombre_tour++;
            this->traiterAction(commande,*(*i),*(*i)->getAdversaires()[0],o);
          }else{
            if(o.IA){
              JoueurIA * test  =((JoueurIA*)  this->liste_joueurs[0]);
              test->jouer(commande,*(*i),*(*i)->getAdversaires()[0],o);
            }else{
              this->traiterAction(commande,*(*i),*(*i)->getAdversaires()[0],o);
            }
          }
          if((*i)->toutCoule()){
           canvas->dessinerTexteEtat(Coordonnee(), "fin du tour" +(*i)->getNom()+" à perdu, gloire à "+(*i)->getAdversaires()[0]->getNom());
           sleep(3);
         }
       }
     }
     canvas->rafraichir();
     sleep(2);
     canvas->dessinerTexteEtat(Coordonnee(), "fin du tour");
     canvas->rafraichir();


   }
//
    //     while((code_touche = getch()) != KEY_F(12))
    //     {   switch(code_touche)
    //         {   case KEY_DOWN:
    //             break;
    //             case KEY_UP:
    //             break;
    //             case KEY_LEFT :
    //             break;
    //             case KEY_RIGHT :
    //             break;

    //     }

    // }

 }  

 void Jeu::jouer_un_tour(int nombre_joueur)
 {
    //TODO

 }


 void Jeu::traiterAction( std::string& chaine, Joueur& j, Joueur& adversaire,const Option& o)
 {
  std::stringstream* ss = new std::stringstream(chaine);
    std::string commande;//la commande poser ou tirer
    std::string param; //le paramatre (tout le temps les coordonnées)
    std::string param2; // le nom du bateau
    std::string param3; //direction
    *ss>>commande>>param>>param2>>param3;   
    std::string prefixe;
    Coordonnee* param_coordonnee = new Coordonnee(param);
    canvas->dessinerTexteEtat(Coordonnee(),param);
    if(commande.compare("tirer") == 0){
     prefixe = "tir en : ";
     if(!(param.compare("") == 0)&& !(commande.compare("") == 0)){
      canvas->dessinerTexteEtat(Coordonnee(),prefixe + param);
      #ifdef DEBUG
      std::cerr<<"nombre bateaux avant tour : "<<j.getListeBateau()->size()<<std::endl;
      #endif
      j.tirer(*param_coordonnee,adversaire);
    }
  }else 
// exemple  :
//poser (1,A) fregate droite
  if(commande.compare("tester") == 0){
    prefixe = "test : ";
    canvas->dessinerTexteEtat(Coordonnee(),prefixe + "yep ca marche");
  }else
  if(commande.compare("poser") == 0){
    prefixe = "pose : ";
    canvas->dessinerTexteEtat(Coordonnee(),prefixe + chaine);
    if(!(param.compare("") == 0)&& !(param2.compare("") == 0)&& !(param3.compare("") == 0)){

      Bateau* b = j.getBateau(param2);
        //choix de direction
      Direction d;
      if(param3.compare("gauche")==0)d =GAUCHE;
      if(param3.compare("droite")==0)d =DROITE;
      if(param3.compare("haut")==0)d =HAUT;
      if(param3.compare("bas")==0)d =BAS;

      canvas->dessinerTexteEtat(Coordonnee(),prefixe + chaine);
      j.positionnnerBateau(*b, *param_coordonnee, d);
    }
  }// exemple  :
//avancer fregate
  if(o.mode_deplacement){
    if(commande.compare("avancer") == 0){
      prefixe = "avance : ";
      canvas->dessinerTexteEtat(Coordonnee(),prefixe + chaine);
      Bateau* b = j.getBateau(param2);
      b->faireAvancer((Grille&) j.GetGrillePersonnel());
 }else// exemple  :
//tourner fregate 1
 if(commande.compare("tourner") == 0){
   prefixe = "tourne : ";
   canvas->dessinerTexteEtat(Coordonnee(),prefixe + chaine);
   Bateau* b = j.getBateau(param2);
   bool direction;
   if(param3.compare("1") == 0){
    direction = true;
  }else if(param3.compare("0") == 0){
    direction = false;
  }
  if(!b->tourner(direction)){
   canvas->dessinerTexteEtat(Coordonnee(),"désolé impossible de tourner");
 }
}
}
else{

  canvas->dessinerTexteEtat(Coordonnee(),"commande non géré");
}
}
