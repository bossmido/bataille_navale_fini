#ifndef GrillePersonnel_h
#define GrillePersonnel_h

#include <vector>

#include "Bateau.h"
#include "Coordonnee.h"
#include "Bateau.h"


class Joueur;

class GrillePersonnel : public Grille {

 public:
    GrillePersonnel();
    GrillePersonnel(int largeur,int hauteur);
    virtual void majGrille();

    virtual void InstallerBateauHasard();

    virtual bool InstallerBateau(Bateau b, Direction d, Coordonnee c);

    virtual void tournerBateau();

    virtual void FaireAvancer();

    void dessiner(Coordonnee base_adresse);
 public:

    /**
     * @element-type Bateau
     */
    std::vector< Bateau > liste_bateaux;
 private:

    Joueur *grille_joueur;
};

#endif // GrillePersonnel_h
