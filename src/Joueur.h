#ifndef Joueur_h
#define Joueur_h

#include <vector>
#include <string>
#include <algorithm>
#include "Bateau.h"
#include "Coordonnee.h"


class GrilleAdverse;
class GrillePersonnel;
class Grille;
class Jeu;
class Joueur {

public:
    Joueur(std::string nom,int largeur,int hauteur,int id);
    Joueur(std::string nom,int largeur,int hauteur,std::vector<Bateau> v,int id);
    void tirer(Coordonnee& position,Joueur& j_ennemi);

    bool positionnnerBateau(Bateau& b, Coordonnee& c,Direction& direction);

    void tourner(Bateau& b,bool sens);
    void dessiner(Coordonnee c);
    void faireAvancer(Bateau& b);
    //test si  le tire qu'à fait l'adversaire tombe sur la position d'un de nos bateau bateau
    bool estTouche(Coordonnee& position);

    GrillePersonnel& GetGrillePersonnel();
    void SetGrillePersonnel(GrillePersonnel arg);

    void ajouterAdversaire(Joueur* amiral_ennemi);
    std::vector< Joueur* >& getAdversaires();
    std::vector<Bateau>* getListeBateau();
    Bateau* getBateau(std::string&);
    void setvalide(bool v);
    bool estvalider();
    std::string getNom() const;
    int getid(){return this->id;}
    void setid(int i){this->id = i;}
    ~Joueur();
    bool toutCoule() const;

protected:
    std::string nom;
    int id;

    /**
     * @element-type GrilleAdverse
     */
     std::vector< GrilleAdverse* > grille_adversaire;

    /**
     * @element-type GrillePersonnel
     */
     GrillePersonnel* grille_joueur;

     std::vector< Joueur* > liste_opposants;

     std::vector< Bateau > liste_bateaux;

     bool aPlaceBateaux;
 };

#endif // Joueur_h
