#ifndef Bateau_h
#define Bateau_h

#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>
class Grille;
#include "Grille.h"
#include "Coordonnee.h"
#include "Dessin.h"
#include "Case.h"
#include "etat.h"




class Bateau : public Dessin {

public:

    Bateau(std::string nom,int l);
    Bateau(const Bateau& ref);
    Bateau(std::string nom,int l,Coordonnee c,Direction d);
    Bateau();

    virtual int getLongueur() const;

    virtual void setLongueur(int l);

    virtual bool estCoule() const;

    virtual void setCoule(bool b);

    virtual Direction getDirection() const;

    virtual void dessiner(Coordonnee base_adresse,Grille& g);

    virtual void setDirection(Direction d);

    virtual void setCoordonnee(Coordonnee c);

    virtual Coordonnee getCoordonnee() const;

    virtual bool tourner(bool);

    virtual bool faireAvancer(Grille& g );

    virtual bool superpose(const std::vector<Bateau>* liste);

    virtual bool superpose(const Bateau& liste);

    virtual Coordonnee coordonneeFin() const;

    virtual std::vector<Case>* calculerListeCase();

    virtual Case getCase(int i)const ;

    virtual Case* getCaseref(int i) ;

    virtual Coordonnee trouvePlace(Grille& g) ;

    virtual std::vector<Case>* getRefCases();
    virtual std::vector<Case> getCases() const;
    virtual void setCases(std::vector<Case>);

    virtual std::string getNom() const ;
    virtual Bateau& operator=(const Bateau& copy);

private:
    int longueur;
    bool coule;
    std::vector<Case> liste_cases;
    Direction direction_bateau;
    Coordonnee coord_bateau;
    std::string nom;


};

#endif // Bateau_h
