#include "Case.h"
#include "Grille.h"

Case::Case()
{
    this->occupe = RIEN;
}

Case::Case(Etatgrille e)
{
    this->occupe = e;
}
Case::Case(const Case& c) 
{
    this->occupe = c.getEtat();
    this->cordonnee_parametre = c.GetCoordonnee();
}


Etatgrille Case::getEtat() const
{
    return this->occupe;
}

void Case::setEtat(Etatgrille e)
{
    this->occupe = e;
}

Case& Case::operator=(const Case& copy){
    this->cordonnee_parametre = copy.GetCoordonnee();
    this->occupe = copy.getEtat();
    return *this;
}

Coordonnee Case::GetCoordonnee() const {
    return this->cordonnee_parametre;
}
void Case::setCoordonnee(Coordonnee arg)
{
    this->cordonnee_parametre = arg;
}
