#include "Fenetre.h"
#include "Fenetreui.h"

Fenetre::Fenetre(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Fenetre)
{

    ui->setupUi(this);
    this->setWindowTitle(QString("jeu de la bataille navale"));
}

Fenetre::~Fenetre()
{
    delete ui;
}
