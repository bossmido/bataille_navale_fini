#ifndef Coordonnee_h
#define Coordonnee_h

#include <math.h>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <ctype.h>
#include "utilitaire.h"
class Coordonnee;
class Bateau;

class Coordonnee {

 public:
    Coordonnee(int X,int Y){
        this->x = X;
        this->y  =Y;
    }
    Coordonnee(): x(1),y(1){}
    Coordonnee(std::string);
    Coordonnee(const Coordonnee& c);
    int getX() const;

    int getY() const;

    void setX(int x);

    void setY(int y);

    int distance(Coordonnee& c2);
    std::string toString();
    int convertirLettreANombre(int n);
    int convertirLettreANombre(std::string n_str);
    bool operator==(const Coordonnee& c);
    Coordonnee& operator=(const Coordonnee& c);

    Coordonnee&  operator-=(const Coordonnee& c2);

    Coordonnee  operator-(const Coordonnee& c);

    Coordonnee&  operator+=(const Coordonnee& c2);

    friend Coordonnee&  operator+(const Coordonnee& c ,const Coordonnee& c2);

    static  bool intersection(const Coordonnee& c1,const Coordonnee& c2, const Coordonnee& c3, const Coordonnee& c4) ;


  private:
    int x;
    int y;

};

#endif // Coordonnee_h
