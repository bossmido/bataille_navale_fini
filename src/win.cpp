#include <sys/time.h>
#include <X11/keysym.h>
#include <string.h>
#include <iostream>
#include "win.h"
#include "event.h"

using std::string;

/******************************************************************************

     FONCTIONS MEMBRES "PUBLIC" : Constructeurs / Destructeurs

 ******************************************************************************/
Win::Win(const char*winame , int w, int h, const char*bg_color)
{
  _create(winame, w,h,bg_color);
}
Win::~Win()
{
  _close();
}

/******************************************************************************

     FONCTIONS MEMBRES "PUBLIC" : Fonctions Graphiques

 ******************************************************************************/
void Win::DrawText(int x, int y, char *text, const char *col, int fnt)
{
	XFontStruct *ft;

	if (fnt==FONT_SMALL) ft = font1; else
	if (fnt==FONT_MEDIUM) ft = font2; else ft = font3;

	XSetForeground(display, gc, get_color(col));

	XSetFont(display, gc, ft->fid);
	XDrawString(display, buffer, gc, x, y+ft->ascent, text, strlen(text));
}
void Win::FillRectangle(int x, int y, int w, int h, const char *col)
{
	XSetForeground(display, gc, get_color(col));
	XFillRectangle(display, buffer, gc, x, y, w, h);
}
void Win::DrawRectangle(int x, int y, int w, int h, const char *col)
{
	XSetForeground(display, gc, get_color(col));
	XDrawRectangle(display, buffer, gc, x, y, w, h);
}
void Win::FillCircle(int x, int y, int w, int h, const char *col)
{
	XSetForeground(display, gc, get_color(col));
	XFillArc(display, buffer, gc, x, y, h, w, 0, 360*64);
}
void Win::DrawCircle(int x, int y, int w, int h, const char *col)
{
	XSetForeground(display, gc, get_color(col));
	XDrawArc(display, buffer, gc, x, y, h, w, 0, 360*64);
}
void Win::DrawLine(int x, int y, int w, int h, const char *col)
{
	XSetForeground(display, gc, get_color(col));
	XDrawLine(display, buffer, gc, x, y, w, h);
}
void Win::DrawWindow()
{
  XSetFillStyle(display, copygc, FillTiled);
  XSetTile(display, copygc, buffer);
  XFillRectangle(display, win, copygc, 0, 0, width, height);
}

void Win::ClearWindow()
{
  //  XClearWindow(display, win);
  XFreePixmap(display,buffer);
  buffer = XCreatePixmap(display, win, width, height, depth);
  /* Set Background White */
  FillRectangle(0,0,width,height,background_color.c_str());
  /* */
  XSetForeground(display, gc, get_color(background_color.c_str()));
  XFillRectangle(display, buffer, gc, 0, 0, width, height);
  /* */
  //  XSetBackground(display, gc, get_color("#ff0000"));
}

/******************************************************************************

     FONCTIONS MEMBRES "PUBLIC" : Fonctions Temps

 ******************************************************************************/
void Win::delay(int frameLen)
{
  int pause,diff;
  struct timeval rt,st;

  gettimeofday(&st, NULL);
  pause = 0;
  gettimeofday(&rt, NULL);
  diff = (1000000*(rt.tv_sec-st.tv_sec))+(rt.tv_usec-st.tv_usec);
  st = rt;
  pause = _delay(frameLen - diff + pause);
  st = rt;
}

/******************************************************************************

     FONCTIONS MEMBRES "PUBLIC" : Fonctions Ev�nements

 ******************************************************************************/
Event Win::getEvent()
{
  XEvent xev;
  Event res;
  //  int i, num_events;
  int num_events;

  //  XFlush(display);
  num_events = XPending(display);
  while((num_events != 0))
    {
      num_events--;
      XNextEvent(display, &xev);
      procEvent(xev, res);
    }
  return res;
}

/******************************************************************************

     FONCTIONS MEMBRES "PRIVATE"

 ******************************************************************************/
void Win::_create(const char *window_name, int Wwidth, int Wheight, const char*bg_color)
{
  //  char *window_name = "Simple Window";
  background_color = bg_color;
  char *icon_name = "window";
  static XSizeHints size_hints;
  Window rootwin;
  unsigned long valuemask = 0;
  XGCValues values;
  
  width=Wwidth;
  height=Wheight;
  display = XOpenDisplay(NULL);
  
  if (display == NULL)
    {
      //		fprintf(stderr, "Failed to open display\n");
      std::cerr << "Failed to open display" << std::endl;
      return;
    }
  
  screen = DefaultScreen(display);
  depth = DefaultDepth(display, screen);
  cmap = DefaultColormap(display, screen);
  
  rootwin = RootWindow(display, screen);
  //	win = XCreateSimpleWindow(display, rootwin, 10, 10, width, height, 5,
  //		BlackPixel(display, screen), BlackPixel(display, screen));

  /* get black and white representation on current screen */
  unsigned long foreground, background;
  background = WhitePixel (display, screen);
  foreground = BlackPixel (display, screen);

  /*  
  win = XCreateSimpleWindow(display, rootwin, 10, 10, width, height, 5,
			    WhitePixel(display, screen), WhitePixel(display, screen));
  */
  
  win = XCreateSimpleWindow(display, rootwin, 10, 10, width, height, 5,
			    foreground, background);
  /*
    win = XCreateSimpleWindow(display, rootwin, 10, 10, width, height, 5,
    foreground, 1000000);
  */  
  XSetWindowColormap(display, win, cmap);
  
  private_colormap();
  
  gc = XCreateGC(display, win, valuemask, &values);
  copygc = XCreateGC(display, win, valuemask, &values);
  
  size_hints.flags = PSize | PMinSize | PMaxSize;
  size_hints.min_width = width;
  size_hints.max_width = width;
  size_hints.min_height = height;
  size_hints.max_height = height;
  
  XSetStandardProperties(display, win, window_name, icon_name, None,
			 0, 0, &size_hints); 
  
  XSelectInput(display, win, ButtonPressMask | ButtonReleaseMask | KeyPressMask);
  XMapWindow(display, win);
  
  buffer = XCreatePixmap(display, win, Wwidth, Wheight, depth);
  
  font1 = XLoadQueryFont(display,
			 "-*-helvetica-bold-r-*-*-14-*-*-*-*-*-iso8859-*");
  font2 = XLoadQueryFont(display,
			 "-*-helvetica-bold-r-*-*-18-*-*-*-*-*-iso8859-*");
  font3 = XLoadQueryFont(display,
			 "-*-helvetica-bold-r-*-*-24-*-*-*-*-*-iso8859-*");
  //
  //  XSetBackground(display, copygc, get_color("WhitePixel"));
  //  XSetBackground(display, copygc, get_color("#00ff00"));
  //  XSetBackground(display, gc, get_color("#00ff00"));
  //  XSetState(display, gc, get_color("#000000"), get_color("#ffffff"), 0, 0);
  //  XClearArea(display, w, x, y, width, height, exposures)
  ClearWindow();

  _pause = 0;
}

void Win::_close()
{
  XFreeGC(display,gc);
  XFreeGC(display,copygc);
  XDestroyWindow(display,win);
  XCloseDisplay(display);
}

void Win::alloc_color(int i, int r, int g, int b)
{
	XColor col;

	col.red = r; col.green = g; col.blue = b;
	col.flags = DoRed | DoGreen | DoBlue;
	if (XAllocColor(display, cmap, &col))
	{
		idx[i].pixel = col.pixel;
	}else
	{
		if (cmap == DefaultColormap(display, screen))
		{
			cmap = XCopyColormapAndFree(display, cmap);
			XSetWindowColormap(display, win, cmap);
			col.red =	r; col.green = g; col.blue = b;
			col.flags = DoRed | DoGreen | DoBlue;
			if (XAllocColor(display, cmap, &col))
			{
				idx[i].pixel = col.pixel;
			}
		}
	}
}

void Win::private_colormap()
{
  int i, j, k;
  //	int r, g, b;
  int count = 0;
  
  for (i=0; i<256; i++) idx[i].pixel = 0;
  
  for (i=0; i<5; i++)
    for (j=0; j<5; j++)
      for (k=0; k<5; k++)
	{
	  idx[count].r = 65535 - (i*16384); if(idx[count].r<0) idx[count].r=0;
	  idx[count].g = 65535 - (j*16384); if(idx[count].g<0) idx[count].g=0;
	  idx[count].b = 65535 - (k*16384); if(idx[count].b<0) idx[count].b=0;
	  alloc_color(count, idx[count].r, idx[count].g, idx[count].b);
	  count++;
	}
  
  for (i=0; i<4; i++)
    for (j=0; j<4; j++)
      for (k=0; k<4; k++)
	{
	  idx[count].r = 60415 - (i*16384); if(idx[count].r<0) idx[count].r=0;
	  idx[count].g = 60415 - (j*16384); if(idx[count].g<0) idx[count].g=0;
	  idx[count].b = 60415 - (k*16384); if(idx[count].b<0) idx[count].b=0;
	  alloc_color(count, idx[count].r, idx[count].g, idx[count].b);
	  count++;
	  idx[count].r = 55295 - (i*16384); if(idx[count].r<0) idx[count].r=0;
	  idx[count].g = 55295 - (j*16384); if(idx[count].g<0) idx[count].g=0;
	  idx[count].b = 55295 - (k*16384); if(idx[count].b<0) idx[count].b=0;
	  alloc_color(count, idx[count].r, idx[count].g, idx[count].b);
	  count++;
	}
}

int Win::get_color(const char *col)
{
  int i, cindx;
  double rd, gd, bd, dist, mindist;
  XColor color;
  //	XColor def_colrs[256];
  
  // create a color from the input string
  XParseColor(display, cmap, col, &color);
  
  // find closest match
  cindx = -1;
  mindist = 196608.0;             // 256.0 * 256.0 * 3.0
  for (i=0; i<256; i++)
    {
      rd = (idx[i].r - color.red) / 256.0;
      gd = (idx[i].g - color.green) / 256.0;
      bd = (idx[i].b - color.blue) / 256.0;
      dist = (rd * rd) + (gd * gd) + (bd * bd);
      if (dist < mindist)
	{
	  mindist = dist;
	  cindx = idx[i].pixel;
	  if (dist == 0.0) break;
	}
    }
  return cindx;
}


void Win::procEvent(XEvent report, Event &evt)
{
  KeySym key;
  
  switch(report.type)
    {
    case KeyPress:
      key = XLookupKeysym(&report.xkey, 0);
      evt.type = KeyPressed;
      switch(key)
	{
	case(XK_KP_4):
	case(XK_Left):
	case(XK_KP_Left):
	  //printf("Left\n");
	  //processKeyLEFT();
	  evt.key = key_left;
	  break;
	case(XK_KP_6):
	case(XK_Right):
	case(XK_KP_Right):
	  //printf("Right\n");
	  //processKeyRIGHT();
	  evt.key = key_right;
	  break;
	case(XK_KP_8):
	case(XK_Up):
	case(XK_KP_Up):
	  //printf("Up\n");
	  //processKeyUP();
	  evt.key = key_up;
	  break;
	case(XK_KP_2):
	case(XK_Down):
	case(XK_KP_Down):
	  //printf("Down\n");
	  //processKeyDOWN();
	  evt.key = key_down;
	  break;
	default: break;
	}
      break;
      
    case ButtonPressMask:
      //printf("You pressed button %d\n", report.xbutton.button);
      switch(report.xbutton.button)
	{
	case 1:
	  evt.type = MB1P;
	  evt.x = report.xbutton.x;
	  evt.y = report.xbutton.y;
	  break;
	case 2:
	  evt.type = MB2P;
	  evt.x = report.xbutton.x;
	  evt.y = report.xbutton.y;
	  break;
	case 3:
	  evt.type = MB3P;
	  evt.x = report.xbutton.x;
	  evt.y = report.xbutton.y;
	  break;
	}
      break;
      
    case ButtonReleaseMask:
      //printf("You pressed button %d\n", report.xbutton.button);
      switch(report.xbutton.button)
	{
	case 1:
	  evt.type = MB1R;
	  evt.x = report.xbutton.x;
	  evt.y = report.xbutton.y;
	  break;
	case 2:
	  evt.type = MB2R;
	  evt.x = report.xbutton.x;
	  evt.y = report.xbutton.y;
	  break;
	case 3:
	  evt.type = MB3R;
	  evt.x = report.xbutton.x;
	  evt.y = report.xbutton.y;
	  break;
	}
      break;
      
    default:
      break;
    }
}

// Fonction utilis� par la fonction delay
int Win::_delay(int i)
{
	struct timeval timeout;

	if (i>0)
	{
		timeout.tv_usec = i % (unsigned long) 1000000;
		timeout.tv_sec = i / (unsigned long) 1000000;
		select(0, NULL, NULL, NULL, &timeout);
	}
	return (i>0 ? i : 0);
}


