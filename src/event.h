#ifndef EVENT_HH
#define EVENT_HH

class Win;

// event_t est le type "�v�nement"
typedef enum { NullEvent, KeyPressed, MB1P, MB2P, MB3P, MB1R, MB2R, MB3R } event_t;
/*
 * Les �v�nements correspondant sont :
 *    - NullEvent  : pas d'�v�nement,
 *    - KeyPressed : touche clavier appuy�,
 *    - MB1P       : bouton 1 de la souris press�,
 *    - MB2P       : bouton 2 de la souris press�,
 *    - MB3P       : bouton 3 de la souris press�,
 *    - MB1R       : bouton 1 de la souris relach�,
 *    - MB2R       : bouton 2 de la souris relach�,
 *    - MB3R       : bouton 3 de la souris relach�,
 */

// kkey_t est le type "touche clavier"
typedef enum { NullKey, key_up, key_down, key_left, key_right } kkey_t;

// Un Objet de type Event m�morise les informations li�es � un �v�nement (clavier ou souris)
class Event
{
 public:
  friend class Win; 
  Event(): type(NullEvent), key(NullKey), x(0), y(0) {}
  // getType() retourne le type de l'�v�nement
  event_t getType() {return type;}
  // getX() retourne la coordonn�e horizontale d'un �v�nement souris 
  int getX() {return x;}
  // getY() retourne la coordonn�e verticale d'un �v�nement souris 
  int getY() {return y;}
  // getKey() renvoie la touche clavier appuyer si celle-ci appartient au type kkey_t
  kkey_t getKey() {return key;}
 private:
  event_t type;
  kkey_t key;
  int x;
  int y;
};

#endif // EVENT_HH

