/********************************************************************************
** Form generated from reading UI file 'Fenetre.ui'
**
** Created: Mon Dec 24 18:41:50 2012
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef FENETREUI_H
#define FENETREUI_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGraphicsView>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Fenetre
{
public:
    QWidget *centralWidget;
    QGraphicsView *graphicsView;
    QDialogButtonBox *buttonBox;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Fenetre)
    {
        if (Fenetre->objectName().isEmpty())
            Fenetre->setObjectName(QString::fromUtf8("Fenetre"));
        Fenetre->resize(294, 400);
        centralWidget = new QWidget(Fenetre);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        graphicsView = new QGraphicsView(centralWidget);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        graphicsView->setGeometry(QRect(20, 0, 256, 311));
        buttonBox = new QDialogButtonBox(centralWidget);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(110, 310, 176, 27));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        Fenetre->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Fenetre);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 294, 25));
        Fenetre->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Fenetre);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        Fenetre->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Fenetre);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Fenetre->setStatusBar(statusBar);

        retranslateUi(Fenetre);

        QMetaObject::connectSlotsByName(Fenetre);
    } // setupUi

    void retranslateUi(QMainWindow *Fenetre)
    {
        Fenetre->setWindowTitle(QApplication::translate("Fenetre", "Fenetre", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Fenetre: public Ui_Fenetre {};
} // namespace Ui

QT_END_NAMESPACE

#endif // FENETREUI_H
