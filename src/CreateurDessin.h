#ifndef CreateurDessin_h
#define CreateurDessin_h

#include <string>

#include "DessinTexte.h"
#include "DessinGUI.h"

class DessinTexte;
class DessinGUI;
class CreateurDessin{
    public:
    static Dessinnable* creer(const std::string& v);
};

#endif