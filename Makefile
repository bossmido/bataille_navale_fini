CC = g++
CC_FLAGS = -w -g
LDFLAG  += -I./include -L./lib -lmenuw -lncursesw -ltinfo
EXEC = bataillenavale

LIBRAIRIE = x11 xext

#############################################################################################################""

PKG = pkg-config
PKG_PARAMETER =  --cflags --libs
PKG_COMMANDE =  $(foreach O,$(LIBRAIRIE),$(PKG) $(O)$  $(PKG_PARAMETER))
LDFLAG += $(shell $(PKG_COMMANDE))
SOURCES = $(wildcard src/*.cpp)
OBJDIR = tmp
OBJECTS = $(SOURCES:.cpp=.o)
FICHIER_TEMP = $(foreach O,$(OBJECTS),$(OBJDIR)/$(notdir $(O)))

OBJS_DEP = $(OBJECTS)


$(EXEC): $(OBJS_DEP)
	$(CC) $(CC_FLAGS) $(FICHIER_TEMP) $(LDFLAG) -o ./bin/$(EXEC)

-include $(FICHIER_TEMP:.o=.d)

test : $(EXEC)
	./bin/$(EXEC)

test_sur : clean $(EXEC)
	./bin/$(EXEC)

%.o: %.cpp
	$(CC) $(CC_FLAGS) $(LDFLAG) -c -o $(OBJDIR)/$(@F)  $<
	$(CC) -MM $(CC_FLAGS)  $(LDFLAG) $*.cpp > $(OBJDIR)/$(notdir $*.d)
	@cp -f $(OBJDIR)/$(notdir $*.d) $(OBJDIR)/$(notdir $*.d.tmp)
	@sed -e 's/.*://' -e 's/\\$$//' < $(OBJDIR)/$(notdir $*.d.tmp) | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $(OBJDIR)/$(notdir $*.d)
	@rm -f $(OBJDIR)/$(notdir $*.d.tmp)



clean:
	rm -f ./bin/$(EXEC) $(FICHIER_TEMP) $(OBJDIR)/*.d