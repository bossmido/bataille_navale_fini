#ifndef Option_h
#define Option_h

struct  Option{
    int nb_joueur;
    int hauteur;
    int largeur;
    bool mode_deplacement;
    bool mode_porte_limite;
    bool rapide;
    bool hasard;
    bool IA;

    Option(){
        this->nb_joueur = 2;
        this->hauteur = 10;
        this->largeur= 10;
        this->mode_deplacement = false;
        this->mode_porte_limite = false;
        this->rapide =  false;
        this->hasard =  false;
        this->IA = false;
    }

};

#endif