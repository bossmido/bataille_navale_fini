#ifndef Menu_h
#define Menu_h

#include <iostream>
#include <vector>
#include <menu.h>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "utilitaire.h"
//#include "Dessinnable.h"
#include "Jeu.h"
#include "Option.h"
using namespace std;

struct Compteur
{
   static int& compteur_entre()
   {
     static int _compteur = 0;
     return _compteur;
   }
};

class Dessinnable;
#include "Dessinnable.h"
extern Dessinnable* canvas;

class Menu{
    private :
    Option choix_option_joueur;
    int longueur_max;
    std::vector<std::string> choix_menu;
    int num_selection;
    public :
    Menu(std::vector<std::string> liste_entre,int nb);
    Menu();
     
    const std::vector<std::string>& getChoixMenu() const{return this->choix_menu;}
    int  getLongueurMax() const{return this->longueur_max;}
    Option& getOption(){return this->choix_option_joueur;}
    void setOption(Option& o){this->choix_option_joueur = o;}
  
    void print();
    void afficher();
};


#endif // Menu_h