#ifndef WIN_HH
#define WIN_HH 1

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include "event.h"

#include <string>

using std::string;


#define FONT_SMALL  1
#define FONT_MEDIUM 2
#define FONT_LARGE  3

typedef struct
{
	int pixel;
	int r, g, b;
}ColorTable;

class Win
{
 public:
  // Constructeur Win | Param�tres : (w:largeur), (h:hauteur)
  Win(const char*winame, int w, int h, const char*bg_color);
  ~Win();
  // Affiche le contenu d'une fen�tre
  void DrawWindow();
  // Efface le contenu d'une fen�tre
  void ClearWindow();
  // Dessine un rectangle plein
  void FillRectangle(int x, int y, int w, int h, const char *col);
  // Dessine un rectangle vide  
  void DrawRectangle(int x, int y, int w, int h, const char *col);
  // Dessine un circle plein
  void FillCircle(int x, int y, int w, int h, const char *col);
  // Dessine un circle vide
  void DrawCircle(int x, int y, int w, int h, const char *col);
  // Dessine une ligne
  void DrawLine(int x, int y, int w, int h, const char *col);
  // Dessine un point
  void DrawPoint(int x, int y);
  // Dessine un texte
  void DrawText(int x, int y, char *text, const char *col, int fnt);
  // Realise un delai
  void delay(int);
  // Renvoi un objet de type Event associ� � la fen�tre
  Event getEvent();

 private:
  unsigned int width, height;
  string background_color;

  Display *display;
  int screen;
  int depth;
  Window win;
  GC gc, copygc;
  Colormap cmap;
  ColorTable idx[256];
  Pixmap buffer;
  XFontStruct *font1, *font2, *font3;

  // Private function members
  void _create(const char*winame, int, int, const char*bg_color);
  void _close();

  void procEvent(XEvent, Event&);
  void private_colormap();
  void alloc_color(int i, int r, int g, int b);
  int  get_color(const char *col);

  int _delay(int);
  int _time_diff();
  int frameLen;
  int _pause;
  struct timeval _st, _rt;
};

#endif // WIN_HH

