#include "GrillePersonnel.h"
#include "GrilleAdverse.h"
#include "Joueur.h"




Joueur::Joueur(std::string nom,int largeur,int hauteur,int id){
    this->nom = nom;
    this->grille_joueur = new GrillePersonnel(largeur,hauteur);
    GrilleAdverse* ga = new GrilleAdverse(largeur,hauteur);
    this->grille_adversaire.push_back(ga);
    this->id = id;

    //initialisation de la liste des bateaux disponibles
    // this->liste_bateaux.push_back(Bateau("Porte_avion", 5));
    // this->liste_bateaux.push_back(Bateau("sous_marin", 4));
    // this->liste_bateaux.push_back(Bateau("fregate", 2));
    // this->liste_bateaux.push_back(Bateau("fregate",2));
    // this->liste_bateaux.push_back(Bateau("couillon sur la mer", 1));

    std::vector<Bateau> liste_bateaux_test;
    Coordonnee* c=  new Coordonnee(3,8);
    Bateau* b =new Bateau("Porte_avion", 5,*c,DROITE);
    liste_bateaux_test.push_back(*b);
    b= new Bateau("fregate",2,Coordonnee(3,5),HAUT);
    liste_bateaux_test.push_back(*b);
    b = new Bateau("fregate", 2,Coordonnee(1,1),HAUT);
    liste_bateaux_test.push_back(*b);

    b =new Bateau("couillon sur la mer", 2,Coordonnee(3,5),DROITE);
    liste_bateaux_test.push_back(*b);
    c = new Coordonnee(4,4);
    b=new  Bateau("sous_marin", 4,*c,DROITE);
    liste_bateaux_test.push_back(*b);

    this->liste_bateaux = liste_bateaux_test;

    aPlaceBateaux = false;
}
Joueur::Joueur(std::string nom,int largeur,int hauteur,std::vector<Bateau> v,int id){
    this->id = id;
    this->nom = nom;
    this->grille_joueur = new GrillePersonnel(largeur,hauteur);
    GrilleAdverse* ga = new GrilleAdverse(largeur,hauteur);
    this->grille_adversaire.push_back(ga);

    //initialisation de la liste des bateaux disponibles
    this->liste_bateaux= v; 
    aPlaceBateaux = true;
}
void Joueur::tirer(Coordonnee& position,Joueur& j_ennemi) 
{
    //par defaut un seul adversaire
    #ifdef DEBUG
    std::cerr<<"tire à la position"<<position.toString()<<std::endl;
    #endif
    if(j_ennemi.estTouche(position)){
        this->grille_adversaire[0]->marquerCase(position,TOUCHE);
    }else{
        this->grille_adversaire[0]->marquerCase(position,ALEAU);
    }
}


void Joueur::dessiner(Coordonnee base_adresse){
    // if((*this).getListeBateau() != NULL){<
    int test = this->grille_joueur->getLargeur();
    Coordonnee nouveau_x = base_adresse+Coordonnee((this->grille_joueur->getLargeur()*4+4)*2+10,10);
    std::vector<Bateau>* nouveau_y = this->getListeBateau();

    for (std::vector<Bateau>::iterator i = this->liste_bateaux.begin(); i != this->liste_bateaux.end();i++)
    {
        (*i).dessiner(base_adresse,*this->grille_joueur);
    }


    // }

    this->grille_joueur->dessiner(base_adresse+Coordonnee(0,this->grille_joueur->getLargeur()*4+8));


    canvas->dessinerListeBateau(nouveau_x,nouveau_y);
    this->grille_adversaire[0]->dessiner(base_adresse+Coordonnee(0,4));
}
bool Joueur::positionnnerBateau(Bateau& b, Coordonnee& c,Direction& direction)
{
    //cas complet de placement

    //1° test de non debordement de la grille

    int x_b = c.getX();
    int y_b = c.getY();
    bool estpossible = false;
    int tmp_etat = static_cast<int>(direction);
    switch(tmp_etat){
        case HAUT : if((y_b>0) && (x_b>0)&& (b.getLongueur()+y_b)< this->grille_joueur->getHauteur()-1 ){estpossible =true;}
        break;
        case DROITE : if((y_b>0) && (x_b>0)&& ((-b.getLongueur())+x_b) > 0 ){estpossible =true;}
        break;
        case BAS :if((y_b>0) && (x_b>0)&& ((-b.getLongueur())+y_b)>0){estpossible =true;}
        break;
        case GAUCHE : if((y_b>0) && (x_b>0)&& (b.getLongueur()+x_b)< this->grille_joueur->getLargeur()-1 ){estpossible =true;}
        break;
    }

    //2° il n'y a pas d'autre bateau sur la grille coincidant avec cette place
    b.setCoordonnee(c);
    b.setDirection(direction);
    // std::vector<Bateau>::iterator ib;
    // for (ib = this->liste_bateaux.begin(); ib != this->liste_bateaux.end() ; ib++)
    // {
    //     if(b.superpose(*ib)){
    //         estpossible = false;
    //     }
    // }
    //TODO 


}



void Joueur::tourner(Bateau& b,bool sens)
{
    b.tourner(sens);
}



void Joueur::faireAvancer(Bateau& b) 
{
    b.faireAvancer(*this->grille_joueur);
}
//
GrillePersonnel& Joueur::GetGrillePersonnel()
{
    return *this->grille_joueur;
}
void Joueur::SetGrillePersonnel(GrillePersonnel arg)
{
    *this->grille_joueur = arg;
}
 // resoudre le probleme de dependance entre la copie des case du bateau et le fait qu'on en puisse pas avoir les coordonnée directement

bool Joueur::estTouche(Coordonnee& position){
    bool esttrouve = false;
    std::vector<Bateau>::iterator i ;
    //std::vector<Case>::iterator j;
    int inc_d = 0;
    #ifdef DEBUG
    std::cerr<<"nombre bateaux avant tour : "<<this->getListeBateau()->size()<<std::endl;
    #endif
    // for ( i = this->liste_bateaux.begin(); i != this->liste_bateaux.end();i++)
    // {
    Bateau* tmp;
    for (int i = 0; i < this->liste_bateaux.size();i++)
    {
            /* code */

        inc_d++;
         #ifdef DEBUG
        std::cerr<<inc_d<<std::endl;
        #endif
        Direction d =  this->liste_bateaux[i].getDirection();
        Coordonnee coord = this->liste_bateaux[i].getCoordonnee();
        int longueur = this->liste_bateaux[i].getLongueur();
        std::vector<Case>* c = this->liste_bateaux[i].getRefCases();
        // tmp =  new  Bateau("fregate",2,Coordonnee(1,1),HAUT);
        // this->liste_bateaux[i] =*tmp;
        int inc = 0;
        Etatgrille etat = TOUCHE;
        Coordonnee* liste_deplacement;
        int x_b = coord.getX()-1;
        int y_b = coord.getY()-1;
        int compte_marque = 0;
        for (int j = 0; j < c->size();j++)
        {

            int tmp_etat = static_cast<int>(d);

            switch(tmp_etat){
                case 0 : liste_deplacement = new Coordonnee(x_b,y_b-inc);if(!esttrouve &&( liste_deplacement->getX() ==  position.getX() && liste_deplacement->getY() ==  position.getY())){ esttrouve = true;c->at(j).setEtat(etat);break;}else{inc--;break;}
                break;
                case 1 : liste_deplacement = new Coordonnee(x_b-inc,y_b) ; if(!esttrouve &&(liste_deplacement->getX() ==  position.getX() && liste_deplacement->getY() ==  position.getY())){ esttrouve = true;c->at(j).setEtat(etat);break;}else{inc--;break;}
                break;
                case 2 : liste_deplacement = new Coordonnee(x_b,y_b+inc);if(!esttrouve &&( liste_deplacement->getX() ==  position.getX() && liste_deplacement->getY() ==  position.getY())){ esttrouve = true;c->at(j).setEtat(etat);break;}else{inc++;break;}
                break;
                case 3 : liste_deplacement = new Coordonnee(x_b+inc,y_b);if(!esttrouve &&( liste_deplacement->getX() ==  position.getX() && liste_deplacement->getY() ==  position.getY())){ esttrouve  =true;c->at(j).setEtat(etat); break;}else{inc++;break;}
                break;
            } 
            delete liste_deplacement;

        }
    }
    std::vector<Case>* test ;
    if(esttrouve){//on modifie la case du bateau quand on l'a trouvé
        //this->grille_joueur->marquerCase(position,TOUCHE);
        return true;
    }else{//sinon on change l'etat de la grille global a TOUCHE aussi
    this->grille_joueur->marquerCase(position,ALEAU);
    return false;
}
}

void Joueur::ajouterAdversaire(Joueur* amiral_ennemi){
    this->liste_opposants.push_back(amiral_ennemi);
}

Bateau* Joueur::getBateau(std::string& nom){
    for (std::vector<Bateau>::iterator i = this->liste_bateaux.begin(); i != this->liste_bateaux.end(); ++i)
    {
        if((*i).getNom().compare(nom) == 0)return &*i;
    }
}
void Joueur::setvalide(bool v){
    this->aPlaceBateaux = v;
}
std::vector<Bateau>* Joueur::getListeBateau(){
    if(!this->liste_bateaux.empty()){
        return &this->liste_bateaux;
    }else{
        return NULL;
    }
}
std::vector< Joueur* >& Joueur::getAdversaires(){
    return this->liste_opposants;
}

bool Joueur::estvalider(){
    return this->aPlaceBateaux;
}
Joueur::~Joueur(){
    delete this->grille_joueur;

}
std::string Joueur::getNom() const{
    return this->nom;
}

bool Joueur::toutCoule() const{
    for (std::vector<Bateau>::const_iterator i = liste_bateaux.begin(); i != liste_bateaux.end(); i++)
    {
        if(!(*i).estCoule())return false;
    }
    return true;
}