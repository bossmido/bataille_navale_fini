#include "Menu.h"
extern Dessinnable* canvas;
Menu::Menu(std::vector<std::string> liste_entre,int nb){

    for (int i = 0; i < liste_entre.size(); i++)
    {
        choix_menu.push_back(liste_entre[i]);
    }
    this->longueur_max = nb;
    canvas->initialiser(this->choix_option_joueur);
}
Menu::Menu(){
    choix_menu.push_back("Jeu rapide");
    choix_menu.push_back("Jeu Hasard");
    choix_menu.push_back("Jouer");
    choix_menu.push_back("choix mode de jeu");
    choix_menu.push_back("choix nombre joueur");
    choix_menu.push_back("Sortie");
    this->longueur_max = 23;
    canvas->initialiser(this->choix_option_joueur);
}

void Menu::print(){
    int inc=0;
    for (std::vector<std::string>::iterator i = choix_menu.begin(); i != choix_menu.end(); ++i)
    {
        if(inc == this->num_selection){
            cout<<">"<<(*i)<<std::endl;
        }else{
            cout<<(*i)<<std::endl;
        }
        inc++;
    }
}

void Menu::afficher(){
   Jeu* jeu_disons_le_moyen_en_fait = NULL;
      #ifdef DEBUG
   choix_option_joueur.nb_joueur = 1;
   choix_option_joueur.largeur = 10;
   choix_option_joueur.hauteur  =10;
    #endif
   bool estvalide = false;
   while(!estvalide){
    int val_teste = canvas->dessinerMenuPrincipal(*this);
    switch(val_teste){
        case 0:
        estvalide= true;
        jeu_disons_le_moyen_en_fait = new Jeu(choix_option_joueur);

        break;
        case 1 :
        canvas->dessinerMenuChoixNombreJoueur(*this);
        canvas->dessinerMenuPrincipal(*this);
        break;
        case 2:
        canvas->dessinerMenuChoixModeJeu(*this);
        canvas->dessinerMenuPrincipal(*this);
        break;
        case 3 :
        canvas->desinitialiser();
        exit(0);
        break;
    }
}
canvas->desinitialiser();
}