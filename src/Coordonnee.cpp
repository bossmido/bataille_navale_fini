#include "Coordonnee.h"

Coordonnee::Coordonnee(std::string s){
    std::vector<std::string> o = split(s,',');
    int inc = 0;
    int coordonnee_gauche = 0;
    int coordonnee_droite = 0;
    #ifdef DEBUG
    std::cerr<<"chaine de caractere à traduire en coordonnee "<<s<<std::endl;
    #endif
    for (std::vector<std::string>::iterator i = o.begin(); i != o.end(); ++i)
    {
        if(inc ==0 ){
            std::vector<std::string> o = split(*i,'(');
                    #ifdef DEBUG
                std::cerr<<"partie gauche "<<o[1][0]<<std::endl;
                    #endif
                std::string base2(o[1].substr(0,1));
                if(!base2.empty()){
                    coordonnee_gauche= convertirLettreANombre(base2.c_str());
                    coordonnee_gauche--;
                    
                }else{coordonnee_gauche =0;}
            }
            if(inc ==1){
                std::vector<std::string> o = split(*i,')');
                #ifdef DEBUG
                std::cerr<<"partie droite "<<o[1][0]<<std::endl;
                #endif
                std::string base(o[1].substr(0,2));
                if (!base.empty())
                {  
                 coordonnee_droite = atoi(base.c_str());
                 coordonnee_droite--;
             }else{
                coordonnee_droite = 0;
            }
            


        }
        inc++;
    }
    this->x = convertirLettreANombre(coordonnee_droite);
    this->y =convertirLettreANombre(coordonnee_gauche); 
}


Coordonnee::Coordonnee(const Coordonnee& c){
    this->x = c.getX();
    this->y = c.getY();


}

int Coordonnee::getX() const
{
    return this->x;
}


int Coordonnee::convertirLettreANombre(int n){
    return n;
}
int Coordonnee::convertirLettreANombre(std::string n_str){
    char c = toupper(n_str.c_str()[0]);
    return (int) c-64;

}
int Coordonnee::getY() const
{
    return y;
}



void Coordonnee::setX(int x)
{
    this->x = x;
}



void Coordonnee::setY(int y)
{
    this->y = y;
}

bool Coordonnee::operator==(const Coordonnee& c){
    bool res = false;
    if(this->x == c.getX() && this->y == c.getY()) res= true;
    return res;
}

int Coordonnee::distance(Coordonnee& c2){
    return ((int) floor(sqrt(pow(this->getX()+c2.getX(),2)+ pow(this->getY()+c2.getY(),2))));
}

Coordonnee& Coordonnee::operator=(const Coordonnee& c){
    this->x = c.getX();
    this->y = c.getY();

    return *this;
}
//
Coordonnee& Coordonnee::operator-=(const Coordonnee& c2){
    Coordonnee * c = new Coordonnee(this->getX()-c2.getX(),this->getY()-c2.getY());
    return *c;
}

Coordonnee  Coordonnee::operator-(const Coordonnee& c){
    return *this-= c;
}


Coordonnee& Coordonnee::operator+=(const Coordonnee& c2){
    Coordonnee * c = new Coordonnee(this->getX()+c2.getX(),this->getY()+c2.getY());
    return *c;
}

Coordonnee&  operator+(const Coordonnee& c ,const Coordonnee& c2){
    int x = c.getX() + c2.getX();
    int y = c.getY() + c2.getY();
    Coordonnee* c3_retour = new Coordonnee(x,y);
    return *c3_retour;
}


bool Coordonnee::intersection(const Coordonnee& c1,const Coordonnee& c2, const Coordonnee& c3, const Coordonnee& c4){
    int x,y=0;
    x = c1.getX() + (c1.getX()-c2.getX())*(c1.getY()-c3.getY())/(c1.getY()-c2.getY());
    if(x >= c3.getX() && x <= c4.getX()){
        if(c1.getY() >= c3.getY() && c2.getY() <= c4.getY()){
            return true;
        }
    }
    return false;
}

std::string Coordonnee::toString(){
    std::string str_ref;
    std::stringstream ss;
    ss<<"("<<this->x<<","<<this->y<<")";
    str_ref = ss.str();
    return str_ref;
}