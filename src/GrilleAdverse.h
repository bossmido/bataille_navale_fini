#ifndef GrilleAdverse_h
#define GrilleAdverse_h

#include <vector>

#include "Coordonnee.h"
class Grille;
#include "Grille.h"
#include "etat.h"

class Joueur;


class GrilleAdverse : public Grille {

public:

    GrilleAdverse();
    GrilleAdverse(int largeur,int hauteur);
    virtual void marquerCase(Coordonnee c, Etatgrille e);

    virtual bool testerCase(Coordonnee c);
    virtual void dessiner(Coordonnee base_adresse);

};

#endif // GrilleAdverse_h
