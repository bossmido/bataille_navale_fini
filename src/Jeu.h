#ifndef Jeu_h
#define Jeu_h

#include <sstream>
#include <unistd.h>
#include <vector>
#include "Couleur.h"
#include "Option.h"
#include "Bateau.h"
#include "JoueurIA.h"

class Joueur;
class Jeu {

 public:
    Jeu(const Option& o);
    void JouerModeTest(const Option& o);
    void JouerMode2Joueur(const Option& o);
    void JouerMode2JoueurRegleModifie();
    void JouerModeX(std::vector<Joueur> liste_joueur);
    void jouer_un_tour(int nombre_joueur);

    void jouer_une_partie_a_deux_joueurs(const Option& o);

    void traiterAction(std::string&,Joueur& j,Joueur& adversaire,const Option& o);

 public:

    /**
     * @element-type Joueur
     */
    std::vector<Joueur*> liste_joueurs;
};

#endif // Jeu_h
