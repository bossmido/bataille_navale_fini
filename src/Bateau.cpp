#include "Bateau.h"
#include "Grille.h"


Bateau::Bateau(std::string nom,int l)
{
    this->longueur = l;
    Case* c;
    for (int i = 0; i < l; ++i)
    {
        c = new Case(NORMAL);
        this->liste_cases.push_back(*c);  
    }

}

bool Bateau::estCoule() const
{
    int inc_valide = 0;
    Etatgrille test_coule = TOUCHE;
    if(this->coord_bateau.getX() != 0 && this->coord_bateau.getY() != 0){
        for (int i = 0; i < this->longueur; i++)
        {
            if(liste_cases[i].getEtat() == test_coule ){
                inc_valide++;
            }
        }
        if (inc_valide == this->longueur)
        {
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }

}

Coordonnee Bateau::trouvePlace(Grille& g){
    bool esttrouve = false;
    this->coord_bateau = Coordonnee();
    int val_hasard = (rand() % g.getLargeur());
    int tmp_etat = static_cast<int>(this->direction_bateau);

 
   while(!esttrouve){
    //placement dans la longueur
        switch(tmp_etat){
            case HAUT : if((this->longueur + val_hasard)<g.getHauteur()){this->coord_bateau.setY(val_hasard);esttrouve = true;}
            break;
            case DROITE : if((this->longueur + val_hasard)<g.getLargeur()){this->coord_bateau.setX(val_hasard);esttrouve = true;}
            break;
            case BAS : if((val_hasard)<g.getHauteur() ){this->coord_bateau.setY(val_hasard);esttrouve = true;}
            break;
            case GAUCHE : if((this->longueur + val_hasard)<g.getLargeur()){this->coord_bateau.setX(val_hasard);esttrouve = true;}
            break;
        }
    //placement sur le côté

        val_hasard = (rand() % g.getLargeur());
        switch(tmp_etat){
            case HAUT : if( val_hasard<g.getLargeur()){this->coord_bateau.setX(val_hasard);esttrouve = true;}
            break;
            case DROITE : if(val_hasard<g.getHauteur()){this->coord_bateau.setY(val_hasard);esttrouve = true;}
            break;
            case BAS : if((val_hasard + this->longueur)<g.getHauteur() ){this->coord_bateau.setX(val_hasard);esttrouve = true;}
            break;
            case GAUCHE : if((val_hasard)<g.getHauteur()){this->coord_bateau.setY(val_hasard);esttrouve = true;}
            break;
        }
    }

}



Bateau::Bateau(const Bateau& ref)
{
    this->coord_bateau = ref.getCoordonnee();
    this->longueur = ref.getLongueur();
    this->direction_bateau = ref.getDirection();
    this->nom =  ref.getNom();
    for (int i = 0; i < ref.getLongueur(); i++)
    {
        this->liste_cases.push_back(ref.getCase(i));
    }

}
Bateau::Bateau()
{
  this->longueur = 2;
  this->nom = "fregate";
  Case* c;
  for (int i = 0; i < longueur; ++i)
  {
    c = new Case(NORMAL);
    this->liste_cases.push_back(*c);  
}}


Bateau::Bateau(std::string nom,int l,Coordonnee c,Direction d){
    this->longueur = l;
    this->nom = nom;
    this->coord_bateau = c;
    this->direction_bateau = d;
    Case* ca;
    for (int i = 0; i < l; ++i)
    {
        ca = new Case(NORMAL);
        this->liste_cases.push_back(*ca);  
    }

}

void Bateau::dessiner(Coordonnee base_dessin,Grille& g){
    canvas->dessinerBateau(base_dessin,*this,g);

}

int Bateau::getLongueur() const 
{
    return this->longueur;
}

void Bateau::setLongueur(int l)
{
    this->longueur = l;
}

void Bateau::setCoule(bool b)
{   
    Etatgrille nouvel_etat = RIEN; 
    if(b){
      nouvel_etat = TOUCHE;
  }
  for (int i = 0; i < this->longueur;i++)
  {
   this->liste_cases[i].setEtat(nouvel_etat);
}
}



Direction Bateau::getDirection() const
{
    return this->direction_bateau;
}



void Bateau::setDirection(const Direction d)
{
    this->direction_bateau = d;
}



void Bateau::setCoordonnee(Coordonnee c)
{
    this->coord_bateau =c;
}



Coordonnee Bateau::getCoordonnee() const
{
    return this->coord_bateau;
}



bool Bateau::tourner(bool direction)
{
    int tmp_etat = static_cast<int>(this->direction_bateau);
    if(direction){
        if(tmp_etat > GAUCHE){
            tmp_etat = HAUT;
        }else{
            tmp_etat++;
        }
    }else{
        if(tmp_etat == HAUT){
            tmp_etat = GAUCHE;
        }else{
            tmp_etat--;
        }
    }
    this->direction_bateau = static_cast<Direction>(tmp_etat);

    //ne prend pas en compte encore des bord de la grille sion le porte serait assez galère
    return true;
}



bool Bateau::faireAvancer(Grille& g) 
{
    bool estpossible = true;
    int x_b = coord_bateau.getX();
    int y_b = coord_bateau.getY();
    Coordonnee* nouvelle_coord = NULL;
    int tmp_etat = static_cast<int>(this->direction_bateau);
    switch(tmp_etat){
        case HAUT : if((y_b+1>0)>(g.getHauteur()-1)){nouvelle_coord= new Coordonnee(x_b,y_b+1);}else{estpossible = false;}
        break;
        case DROITE : if((x_b+1)<(g.getLargeur()-1)){nouvelle_coord= new Coordonnee(x_b+1,y_b);}else{estpossible = false;}
        break;
        case BAS :if(y_b-1>0){nouvelle_coord= new Coordonnee(x_b,y_b-1);}else{estpossible = false;}
        break;
        case GAUCHE :if(x_b-1>0){nouvelle_coord= new Coordonnee(x_b-1,y_b);}else{estpossible = false;}
        break;
    }
    //this->majCases(g);
    this->coord_bateau = *nouvelle_coord;
    return estpossible;
}

bool Bateau::superpose(const std::vector<Bateau>* liste){
    std::vector<Bateau>::const_iterator it;
    bool paspossible = false;
    for(it = liste->begin(); it != liste->end();++it)
    {
        if(this->superpose(*it)){
            paspossible  =true;
        }   
    }
    return paspossible;
}

bool Bateau::superpose(const Bateau& liste){
    return Coordonnee::intersection(this->coord_bateau,this->coordonneeFin(),liste.getCoordonnee(),liste.coordonneeFin());
}

Coordonnee Bateau::coordonneeFin() const{
    int tmp_etat = this->direction_bateau;
    Coordonnee* res = NULL;
    int x_b = this->coord_bateau.getX();
    int y_b = this->coord_bateau.getY();
    switch(tmp_etat){
        case HAUT : res  =new Coordonnee(x_b,y_b + this->longueur);
        break;
        case DROITE :  res  =new Coordonnee(x_b + this->longueur,y_b);
        break;
        case BAS :  res  =new Coordonnee(x_b,y_b - this->longueur);
        break;
        case GAUCHE :  res  =new Coordonnee(x_b - this->longueur,y_b);
        break;
        default : res = new Coordonnee(0,0);
        break;
    } 
    return *res;
}
Case Bateau::getCase(int i) const{
    if(this->liste_cases.size()>i){
        return this->liste_cases[i];
    }else{
        return Case();
    }
}
std::vector<Case>* Bateau::calculerListeCase(){
    int tmp_etat = this->direction_bateau;
    std::vector<Case>* v =new std::vector<Case>() ;
    Case* cas;
    int x_b = coord_bateau.getX();
    int y_b = coord_bateau.getY();
    Coordonnee* tmp_coord  =NULL;
    for (int i = 0; i < this->getLongueur(); i++)
    {

        switch(tmp_etat){
            case HAUT : 
            tmp_coord = new Coordonnee(x_b,y_b++);
            cas = this->getCaseref(i);
            cas->setCoordonnee(*tmp_coord);
            v->push_back(*cas);
            break;
            case DROITE :   tmp_coord = new Coordonnee(x_b++,y_b);
            cas = this->getCaseref(i);
            cas->setCoordonnee(*tmp_coord);
            v->push_back(*cas);
            break;
            case BAS :  tmp_coord = new Coordonnee(x_b,y_b--);
            cas = this->getCaseref(i);
            cas->setCoordonnee(*tmp_coord);
            v->push_back(*cas);
            break;
            case GAUCHE : tmp_coord = new Coordonnee(x_b++,y_b);
            cas = this->getCaseref(i);
            cas->setCoordonnee(*tmp_coord);
            v->push_back(*cas);
            break;
        } 

    }
    return v;

}


std::vector<Case>* Bateau::getRefCases(){
    return &this->liste_cases;
}

void Bateau::setCases(std::vector<Case> v){
    this->liste_cases = v;
}

std::string  Bateau::getNom() const {
    return this->nom;
}

Case* Bateau::getCaseref(int i){
    return &this->liste_cases[i];
}
std::vector<Case> Bateau::getCases() const {
    return this->liste_cases;
}

Bateau& Bateau::operator=(const Bateau& copy){
    this->longueur = copy.getLongueur();
    //this->coule = 
    this->liste_cases = copy.getCases();
    this->direction_bateau = copy.getDirection();
    this->coord_bateau = copy.getCoordonnee();
    this->nom = copy.getNom();
    return *this;
}
