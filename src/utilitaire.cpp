#include "utilitaire.h"




const char** constchar(const std::vector<std::string>& arg)
{
    std::vector<char const*>* v = new std::vector<char const*>( arg.size() );
    for( size_t i = 0; i < v->size(); ++i ) { v->at(i) = arg[i].c_str(); }
    return v->data();
}

void print_in_middle(WINDOW *win, int starty, int startx, int width, char *string, chtype color)
{   int length, x, y;
    float temp;

    if(win == NULL)
        win = stdscr;
    getyx(win, y, x);
    if(startx != 0)
        x = startx;
    if(starty != 0)
        y = starty;
    if(width == 0)
        width = 80;

    length = strlen(string);
    temp = (width - length)/ 2;
    x = startx + (int)temp;
    wattron(win, color);
    mvwprintw(win, y, x, "%s", string);
    wattroff(win, color);
    refresh();
}

int largeur_terminal()
{
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    return(w.ws_col);
}

int hauteur_terminal()
{
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    return(w.ws_row);
}


WINDOW *create_newwin(int height, int width, int starty, int startx)
{   WINDOW *local_win;

    local_win = newwin(height, width, starty, startx);


    wborder(local_win,
            ACS_VLINE |COLOR_PAIR(COLOR_BLACK),
            ACS_VLINE |COLOR_PAIR(COLOR_BLACK),
            ACS_HLINE|COLOR_PAIR(COLOR_BLACK),
            ACS_HLINE|COLOR_PAIR(COLOR_BLACK),
            ACS_ULCORNER|COLOR_PAIR(COLOR_BLACK),
            ACS_URCORNER|COLOR_PAIR(COLOR_BLACK),
            ACS_LLCORNER|COLOR_PAIR(COLOR_BLACK),
            ACS_LRCORNER|COLOR_PAIR(COLOR_BLACK));
    box(local_win, 0 , 0);      /* 0, 0 gives default characters 
                     * for the vertical and horizontal
                     * lines            */
    wrefresh(local_win);        /* Show that box        */

    return local_win;
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while(std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    return split(s, delim, elems);
}