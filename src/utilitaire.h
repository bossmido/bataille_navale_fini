#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <ncurses.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
    #define colored_ctype(ch, attr, pair) \
        ((ch) | (attr) | COLOR_PAIR(pair))
        
const char** constchar(const std::vector<std::string>& arg_);

void print_in_middle(WINDOW *win, int starty, int startx, int width, char *string, chtype color);

int largeur_terminal();
int hauteur_terminal();

WINDOW *create_newwin(int height, int width, int starty, int startx);


std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);


std::vector<std::string> split(const std::string &s, char delim);