#ifndef DessinTexte_h
#define DessinTexte_h

#include <ncurses.h>
//GG pour la macro vous m'avez bien pourri la vie avec le conflit avec Qt salaud
inline int ncurses_scroll(WINDOW *w) { scroll(w); }
#ifdef scroll
# undef scroll
#endif

#include <menu.h>
#include <cstdlib>
#include <stdio.h>
#include <cstring>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "utilitaire.h"

#include "Dessinnable.h"

#define CURSE_ROUGE 1
#define CURSE_JAUNE 2
#define CURSE_BLEU 3


class DessinTexte : public Dessinnable{
    private :
    WINDOW *grid;
    WINDOW *grid_adverse;
    WINDOW* win_liste_bateau;
    WINDOW* win_debug;
    int largeur;
    int hauteur;
public:
    DessinTexte(int largeur, int hauteur){
        this->config = new ConfigDessin(hauteur,largeur);
    }
    void initialiser(Option& o){
        setlocale(LC_ALL, "");
        initscr();
        cbreak();
        noecho();
        start_color();

        init_color(COLOR_RED, 700, 0, 0);
        this->hauteur = hauteur;
        this->largeur = largeur;
        init_pair(CURSE_ROUGE, COLOR_RED, COLOR_BLACK);
        init_pair(CURSE_JAUNE, COLOR_YELLOW, COLOR_BLACK);
        init_pair(CURSE_BLEU, COLOR_BLUE, COLOR_BLACK);

        int starty = 5;  
        int startx = 6;  
        this->grid = create_newwin(o.hauteur*2+2, o.largeur*4+1, starty+4, startx+o.largeur*4+4);
        this->grid_adverse = create_newwin(o.hauteur*2+2, o.largeur*4+1, starty+4, startx+0);
        //keypad(stdscr, TRUE);
    }

    int dessinerMenuPrincipal( Menu& m){
        ITEM **my_items = NULL;
        int c;              
        MENU *my_menu = NULL;
        int n_choices, i;
        ITEM *cur_item = NULL;
        std::vector<string> v  = m.getChoixMenu();
        const char **choices =constchar(v);
    #ifdef DEBUG
        mvprintw(0, 0, "MODE DEBUG");
    #endif


        n_choices = v.size();
        my_items = (ITEM **)calloc(n_choices + 1, sizeof(ITEM *));

        for(i = 0; i < n_choices; ++i)
            my_items[i] = new_item(choices[i], choices[i]);
        my_items[n_choices] = (ITEM *)NULL;
    //crée un menu
        my_menu = new_menu((ITEM **)my_items);
    //pas de description de choix
        menu_opts_off(my_menu, O_SHOWDESC);
    /* Create the window to be associated with the menu */
        WINDOW *win_menu;
        int hauteur = hauteur_terminal();
        int largeur = largeur_terminal();
        win_menu = newwin(hauteur-3,largeur-2, 1, 1);
        keypad(win_menu, TRUE);

    /* Set main window and sub window */
        set_menu_win(my_menu, win_menu);
        int milieu_boite = (largeur-2)/2-(m.getLongueurMax()/2);
        set_menu_sub(my_menu, derwin(win_menu, 6, 37, 3,milieu_boite));

    /* Set menu mark to the string " * " */
        set_menu_mark(my_menu, " > ");

        box(win_menu, 0, 0);
        print_in_middle(win_menu, 1, 0,largeur, "Menu", COLOR_PAIR(1));
        mvwaddch(win_menu, 2, 0, ACS_LTEE);
        mvwhline(win_menu, 2, 1, ACS_HLINE, largeur-5);
        mvprintw(LINES - 2, 0, "F12 pour quitter");


           /* Post the menu */
        post_menu(my_menu);
        wrefresh(win_menu);        
        refresh();    
        while((c = wgetch(win_menu)) != KEY_F(12))
            {   switch(c)
                {   case KEY_DOWN:
                    menu_driver(my_menu, REQ_DOWN_ITEM);
                    break;
                    case KEY_UP:
                    menu_driver(my_menu, REQ_UP_ITEM);
                    break;
                    case 10 :
                    move(LINES-1, 0);
                    clrtoeol();
                    mvprintw(LINES-1, 0, "choix sélectionné : %s", 
                        item_name(current_item(my_menu)));
                    move(0, 0);
                    if(!strcmp(item_name(current_item(my_menu)),"Sortie")){
                      unpost_menu(my_menu);
                      free_menu(my_menu);
                      for(i = 0; i < n_choices; ++i)
                        free_item(my_items[i]);
                    endwin();
                    return 3;
                }else if(!strcmp(item_name(current_item(my_menu)),"choix mode de jeu")){
                    unpost_menu(my_menu);
                    free_menu(my_menu);
                    werase(win_menu);
                    for(i = 0; i < n_choices; ++i)
                        free_item(my_items[i]);
                    refresh();
                    return 2;
                    
                }else if(!strcmp(item_name(current_item(my_menu)),"choix nombre joueur")){
                    unpost_menu(my_menu);
                    free_menu(my_menu);
                    werase(win_menu);
                    for(i = 0; i < n_choices; ++i)
                        free_item(my_items[i]);
                    refresh();
                    return 1;
                    
                }else if(!strcmp(item_name(current_item(my_menu)),"Jouer")){
                    unpost_menu(my_menu);
                    free_menu(my_menu);
                    werase(win_menu);
                    for(i = 0; i < n_choices; ++i)
                        free_item(my_items[i]);
                    refresh();
                    return 0;
                    
                }else if(!strcmp(item_name(current_item(my_menu)),"Jeu rapide")){
                    unpost_menu(my_menu);
                    free_menu(my_menu);
                    werase(win_menu);
                    for(i = 0; i < n_choices; ++i)
                        free_item(my_items[i]);
                    m.getOption().rapide = true;
                    refresh();
                    return 0;
                }else if(!strcmp(item_name(current_item(my_menu)),"Jeu Hasard")){
                    unpost_menu(my_menu);
                    free_menu(my_menu);
                    werase(win_menu);
                    for(i = 0; i < n_choices; ++i)
                        free_item(my_items[i]);
                    m.getOption().rapide = true;
                    m.getOption().hasard = true;
                    refresh();
                    return 0;
                }

            }
        }
        unpost_menu(my_menu);
        free_menu(my_menu);
        for(i = 0; i < n_choices; ++i)
            free_item(my_items[i]);
        endwin();

    }
    

    void dessinerMenuChoixNombreJoueur(Menu& m){
        ITEM **my_items = NULL;
        int c;              
        MENU *my_menu = NULL;
        int n_choices, i;
        ITEM *cur_item = NULL;
        std::vector<string> v;
        v.push_back("2");
        v.push_back("3");
        v.push_back("4");
        v.push_back("Sortie");
        const char **choices =constchar(v);
    #ifdef DEBUG
        mvprintw(0, 0, "MODE DEBUG");
    #endif


        n_choices = v.size();
        my_items = (ITEM **)calloc(n_choices + 1, sizeof(ITEM *));

        for(i = 0; i < n_choices; ++i)
            my_items[i] = new_item(choices[i], choices[i]);
        my_items[n_choices] = (ITEM *)NULL;
    //crée un menu
        my_menu = new_menu((ITEM **)my_items);
    //pas de description de choix
        menu_opts_off(my_menu, O_SHOWDESC);
    /* Create the window to be associated with the menu */
        WINDOW *win_menu;
        int hauteur = hauteur_terminal();
        int largeur = largeur_terminal();
        win_menu = newwin(hauteur-3,largeur-2, 1, 1);
        keypad(win_menu, TRUE);

    /* Set main window and sub window */
        set_menu_win(my_menu, win_menu);
        int milieu_boite = (largeur-2)/2-(m.getLongueurMax()/2);
        set_menu_sub(my_menu, derwin(win_menu, 6, 37, 3,milieu_boite));

    /* Set menu mark to the string " * " */
        set_menu_mark(my_menu, " > ");

        box(win_menu, 0, 0);
        print_in_middle(win_menu, 1, 0,largeur, "Menu", COLOR_PAIR(1));
        mvwaddch(win_menu, 2, 0, ACS_LTEE);
        mvwhline(win_menu, 2, 1, ACS_HLINE, largeur-5);
        mvprintw(LINES - 2, 0, "F12 pour quitter");
        //affichage du choix par defaut
        char str_option[200];
        sprintf(str_option,"choix déjà défini : %d",m.getOption().nb_joueur);
        mvprintw(2, 2,str_option);


           /* Post the menu */
        post_menu(my_menu);
        wrefresh(win_menu);        
        refresh();    
        while((c = wgetch(win_menu)) != KEY_F(12))
            {   switch(c)
                {   case KEY_DOWN:
                    menu_driver(my_menu, REQ_DOWN_ITEM);
                    break;
                    case KEY_UP:
                    menu_driver(my_menu, REQ_UP_ITEM);
                    break;
                    case 10 :
                    move(LINES-1, 0);
                    clrtoeol();
                    mvprintw(LINES-1, 0, "choix sélectionné : %s", 
                        item_name(current_item(my_menu)));
                    move(0, 0);
                    if(!strcmp(item_name(current_item(my_menu)),"1")){
                        m.getOption().nb_joueur = 1;
                        unpost_menu(my_menu);
                        free_menu(my_menu);
                        for(i = 0; i < n_choices; ++i)
                            free_item(my_items[i]);
                        endwin();
                        return;
                    }else if(!strcmp(item_name(current_item(my_menu)),"2")){
                     m.getOption().nb_joueur = 2;
                     unpost_menu(my_menu);
                     free_menu(my_menu);
                     for(i = 0; i < n_choices; ++i)
                        free_item(my_items[i]);
                    endwin();
                    return;
                }if(!strcmp(item_name(current_item(my_menu)),"3")){
                 m.getOption().nb_joueur = 3;
                 unpost_menu(my_menu);
                 free_menu(my_menu);
                 for(i = 0; i < n_choices; ++i)
                    free_item(my_items[i]);
                endwin();
                return;
            }if(!strcmp(item_name(current_item(my_menu)),"4")){
             m.getOption().nb_joueur = 4;
             unpost_menu(my_menu);
             free_menu(my_menu);
             for(i = 0; i < n_choices; ++i)
                free_item(my_items[i]);
            endwin();
            return;
        }if(!strcmp(item_name(current_item(my_menu)),"Sortie")){
          unpost_menu(my_menu);
          free_menu(my_menu);
          for(i = 0; i < n_choices; ++i)
            free_item(my_items[i]);
        endwin();
        return;
    }

}
}
unpost_menu(my_menu);
free_menu(my_menu);
for(i = 0; i < n_choices; ++i)
    free_item(my_items[i]);
endwin();
}
void dessinerMenuChoixModeJeu(Menu& m){

 ITEM **my_items = NULL;
 int c;              
 MENU *my_menu = NULL;
 int n_choices, i;
 ITEM *cur_item = NULL;
 std::vector<string> v;
 v.push_back("mode de jeu simple");
 v.push_back("mode de jeu deplacement");
 //v.push_back("mode de jeu deplacement et porté limité");
 v.push_back("mode IA");
 v.push_back("Sortie");
 const char **choices =constchar(v);
    #ifdef DEBUG
 mvprintw(0, 0, "MODE DEBUG");
    #endif


 n_choices = v.size();
 my_items = (ITEM **)calloc(n_choices + 1, sizeof(ITEM *));

 for(i = 0; i < n_choices; ++i)
    my_items[i] = new_item(choices[i], choices[i]);
my_items[n_choices] = (ITEM *)NULL;
    //crée un menu
my_menu = new_menu((ITEM **)my_items);
    //pas de description de choix
menu_opts_off(my_menu, O_SHOWDESC);
    /* Create the window to be associated with the menu */
WINDOW *win_menu;
int hauteur = hauteur_terminal();
int largeur = largeur_terminal();
win_menu = newwin(hauteur-3,largeur-2, 1, 1);
keypad(win_menu, TRUE);

    /* Set main window and sub window */
set_menu_win(my_menu, win_menu);
int milieu_boite = (largeur-2)/2-(m.getLongueurMax()/2);
set_menu_sub(my_menu, derwin(win_menu, 6, 37, 3,milieu_boite));

    /* Set menu mark to the string " * " */
set_menu_mark(my_menu, " > ");

box(win_menu, 0, 0);
print_in_middle(win_menu, 1, 0,largeur, "Menu", COLOR_PAIR(3));
mvwaddch(win_menu, 2, 0, ACS_LTEE);
mvwhline(win_menu, 2, 1, ACS_HLINE, largeur-5);
mvprintw(LINES - 2, 0, "F12 pour quitter");
//choix d'option déjà définie
char str_option[200];
if(m.getOption().mode_deplacement) mvprintw(2, 2,"mode déplacemement");
if(m.getOption().mode_porte_limite) mvprintw(2, 2,"mode déplacmement + porté");
if(!m.getOption().mode_porte_limite && !m.getOption().mode_deplacement ) mvprintw(2, 2,"mode : classique");
           /* Post the menu */
post_menu(my_menu);
wrefresh(win_menu);        
refresh();    
while((c = wgetch(win_menu)) != KEY_F(12))
    {   switch(c)
        {   case KEY_DOWN:
            menu_driver(my_menu, REQ_DOWN_ITEM);
            break;
            case KEY_UP:
            menu_driver(my_menu, REQ_UP_ITEM);
            break;
            case 10 :
            move(LINES-1, 0);
            clrtoeol();
            mvprintw(LINES-1, 0, "choix sélectionné : %s", 
                item_name(current_item(my_menu)));
            move(0, 0);
                    if(!strcmp(item_name(current_item(my_menu)),v[0].c_str())){// test mode jeu simple
                        m.getOption().mode_deplacement = false;
                        m.getOption().mode_porte_limite= false;
                        unpost_menu(my_menu);
                        free_menu(my_menu);
                        for(i = 0; i < n_choices; ++i)
                            free_item(my_items[i]);
                        endwin();
                        return;
                }else  if(!strcmp(item_name(current_item(my_menu)),v[1].c_str())){// test mode jeu deplacement
                    m.getOption().mode_deplacement = true;
                    m.getOption().mode_porte_limite= false;
                    unpost_menu(my_menu);
                    free_menu(my_menu);
                    for(i = 0; i < n_choices; ++i)
                        free_item(my_items[i]);
                    endwin();
                    return;
                }else  if(!strcmp(item_name(current_item(my_menu)),v[2].c_str())){// test mode jeu deplacement avec porté limitée
                    m.getOption().mode_deplacement = false;
                    m.getOption().mode_porte_limite= true;
                    unpost_menu(my_menu);
                    free_menu(my_menu);
                    for(i = 0; i < n_choices; ++i)
                        free_item(my_items[i]);
                    endwin();
                    return;
                }

            }
        }
        unpost_menu(my_menu);
        free_menu(my_menu);
        for(i = 0; i < n_choices; ++i)
            free_item(my_items[i]);
        endwin();
    }
    void dessinerBateau(Coordonnee c,Bateau& b,Grille& g){
        std::vector<Case>* v = b.calculerListeCase();//mais quel con je te jure...normal que les acases ne changait pas d'état
        std::vector<Case>* vb = b.getRefCases();
        for (int k = 0; k < v->size(); k++)
        {
         v->at(k).setEtat(vb->at(k).getEtat());
           #ifdef DEBUG
         std::cerr<<"aaaaaaa case etat bateau :"<<vb->at(k).getEtat()<<std::endl<<std::endl;
           #endif
     }        
     Coordonnee vide;

     int inc_test = 0;
     for (std::vector<Case>::iterator i = v->begin(); i != v->end(); i++)
     {
        if(inc_test == 0 && (b.getDirection() == HAUT || b.getDirection() == GAUCHE)){
            this->dessinerCasePremier(vide,(*i),g,b); 
        }else if(inc_test == v->size()-1 && (b.getDirection() == BAS || b.getDirection() == DROITE)){
         this->dessinerCaseDernier(vide,(*i),g,b); 
     }else{
         this->dessinerCase(vide,(*i),g); 
     }
     inc_test++;
 }
}
void dessinerCase(Coordonnee c,Case& cases,Grille& g){
    Type_grille jj = JOUEUR;
    Type_grille aa = ADVERSAIRE;
    Type_grille au = AUCUN;
    int deplacement = 0;
    WINDOW* grid_tmp = NULL;
    if(g.getType() == jj){
       grid_tmp = this->grid;
       deplacement = 0;
   }else if(g.getType() == aa){
    deplacement = 0;
    grid_tmp = this->grid_adverse;
}

Coordonnee c2 = g.getCoordonnee();
int indice_x = 0;
int indice_y =0;
int couleur=0;
std::string carac_affiche = "x";
if(cases.getEtat() == RIEN){
    return;
}
if(cases.getEtat() == ALEAU){
    couleur = CURSE_JAUNE;
}
if(cases.getEtat() == NORMAL){
    couleur = CURSE_BLEU;
}
if(cases.getEtat() == TOUCHE){
    couleur = CURSE_ROUGE;
    carac_affiche = "x";
}
if(g.getType() == jj)
     #ifdef DEBUG
    std::cerr<<"etat de la case bateau au dessin : "<<cases.getEtat()<<" au x:"<<cases.GetCoordonnee().getX()<<" y:"<<cases.GetCoordonnee().getY()<<std::endl;
    #endif
indice_x = 0;
indice_y = 0;

indice_x = ((cases.GetCoordonnee().getX())*4)-2;
indice_y = ((cases.GetCoordonnee().getY()-1)*2)+1;

wattron (grid_tmp, COLOR_PAIR (couleur));

mvwprintw(grid_tmp,indice_y,indice_x,carac_affiche.c_str());
wrefresh(grid_tmp);
refresh(); 
wattroff (grid_tmp,COLOR_PAIR(couleur));


}
void dessinerCaseExtremum(Coordonnee c, Case& cases,Grille& g,Bateau& bateau){
    Type_grille j = JOUEUR;
    Type_grille a = ADVERSAIRE;
    WINDOW* grid_tmp = NULL;
    grid_tmp = this->grid;
    Coordonnee c2 = g.getCoordonnee();
    int indice_x = 0;
    int indice_y =0;
    int couleur=0;
    if(cases.getEtat() == ALEAU){
        couleur = CURSE_JAUNE;
    }
    if(cases.getEtat() == NORMAL){
        couleur = CURSE_BLEU;
    }
    if(cases.getEtat() == TOUCHE){
        couleur = CURSE_ROUGE;
}               // KK
indice_x = ((cases.GetCoordonnee().getX())*4)-2;
indice_y = ((cases.GetCoordonnee().getY()-1)*2) +1;
int direction_entier = bateau.getDirection();
wattron (grid_tmp, COLOR_PAIR (couleur));
std::string sortie ="";
switch(direction_entier){
    case 0:    sortie.append(CARAC_HAUT);mvwprintw(grid_tmp,indice_y,indice_x,sortie.c_str());
    break;
    case 1:    sortie.append(CARAC_DROITE);mvwprintw(grid_tmp,indice_y,indice_x,sortie.c_str());
    break;
    case 2:    sortie.append(CARAC_BAS);mvwprintw(grid_tmp,indice_y,indice_x,sortie.c_str());
    break;
    case 3:    sortie.append(CARAC_GAUCHE);mvwprintw(grid_tmp,indice_y,indice_x,sortie.c_str());
    break;
}
wrefresh(grid_tmp);
refresh(); 
wattroff (grid_tmp,COLOR_PAIR(couleur));


}
void dessinerCaseDernier(Coordonnee c, Case& cases,Grille& g,Bateau& bateau){
    dessinerCaseExtremum(c,cases,g,bateau);
}
void dessinerCasePremier(Coordonnee c,Case& cases,Grille& g,Bateau& bateau){
    dessinerCaseExtremum(c,cases,g,bateau);
}
void dessinerCurseur(Coordonnee c,const  Grille& g){}

void dessinerGrille(Coordonnee c,Grille& g){
   int i, j;
   int left;
   Type_grille jj = JOUEUR;
   Type_grille aa = ADVERSAIRE;
   Type_grille au = AUCUN;
   WINDOW* grid_tmp = NULL;
   if(g.getType() == jj){

    grid_tmp = this->grid;
}else if(g.getType() == aa){

 grid_tmp = this->grid_adverse;
}

int starty = 5;  
int startx = 6;  
Coordonnee* coord_tmp = new Coordonnee(c.getX()+startx,starty+c.getY());
g.setCoordonnee(*coord_tmp);


      /*horizontal*/
wchar_t vertical = L'─';
wchar_t horizontal = L'|';
      /* verticales */
for (i=0; i<g.getHauteur()*4+4; i+=4) {
 mvwvline(grid_tmp, 1, i, ACS_VLINE, g.getHauteur()*2+4);
}
for (i=0; i<g.getLargeur()*4+4; i+=2) {
 mvwhline(grid_tmp, i, 1, ACS_HLINE, g.getLargeur()*4+2);
}
wmove(grid_tmp,0,0);
for (i=0; i<g.getLargeur()*8; i+=4) {
    for (int j=0; j<g.getHauteur()*4+2; j+=2) {
        wmove(grid_tmp,j,i);
        wprintw(grid_tmp," ");
    }
}

      /* indices pour joueur horizontaux */
Coordonnee* tmp_indice = NULL;

if(g.getType() == jj){
 tmp_indice = new Coordonnee(25,0);

}else{
   tmp_indice =new Coordonnee();
}
tmp_indice->setX(starty+c.getY()-6);
tmp_indice->setY(startx+c.getX()+4);
for (int m = 1; m < 11; m++)
{

 mvprintw((*tmp_indice).getY(),(*tmp_indice).getX(),"%c",m+64);
 tmp_indice->setY(tmp_indice->getY()+2);

}
        /* indices pour joueur verticaux */
Coordonnee* tmp_indice2; 
if(g.getType() == jj){
   tmp_indice2  =new Coordonnee(starty+c.getY()-1, startx+c.getX()+2);
}else{
   tmp_indice2 = new Coordonnee(starty+c.getY()-1, startx+c.getX()+2);
}
wattron (grid_tmp, COLOR_PAIR (CURSE_JAUNE));
for (int m = 1; m < 11; m++)
{
 mvprintw((*tmp_indice2).getY(),(*tmp_indice2).getX(),"%d",m);
 tmp_indice2->setX(tmp_indice2->getX()+4);
}

wattroff (grid_tmp,COLOR_PAIR(CURSE_JAUNE));

       /* dessine les états des cases de la grilles adverses */
Coordonnee* coord_par_cases = NULL;
int compteur_y_cases =0;
int compteur_x_cases =0;

std::vector<std::vector<Case> >& vcases = g.getCases();
for (std::vector<std::vector<Case> >::iterator k = vcases.begin(); k != vcases.end(); k++)
{
    compteur_y_cases++;
    compteur_x_cases =0;
    for (std::vector<Case>::iterator l = k->begin(); l != k->end(); l++)
    {
        compteur_x_cases++;
        coord_par_cases =  new Coordonnee(compteur_y_cases,compteur_x_cases);
        (*l).setCoordonnee(*coord_par_cases);
        this->dessinerCase(Coordonnee(),(*l),g);
        delete coord_par_cases;
        //std::cerr<<"passe cases x:"<<compteur_x_cases<<" y:"<<compteur_y_cases<<std::endl;
    }
}

      /* Horizontal *///
redrawwin(grid_tmp);

wrefresh(grid_tmp);        

}



             //TODO
void dessinerTexteTour(Coordonnee c,const std::string&  tour){
    int starty = 5;  
    int startx = 6;  
    mvprintw(starty + hauteur*4,startx,tour.c_str());
    refresh();
    
}
void dessinerTexteGrilleAdverse(Coordonnee c,const std::string& tour){}


void desinitialiser(){
    endwin();
}

virtual void dessinerGrilleJoueur(Coordonnee c,Grille& g){
    dessinerGrille(c,g);
}
virtual void dessinerGrilleAdversaire(Coordonnee c,Grille& g){
    dessinerGrille(c,g);
}

virtual std::string getEntre(){
    mvprintw(LINES-5, 3, ">"); 
    move(LINES-5, 3);
    char buff[50]={};
    char buff2[50];
    char c_tmp;
    int compte = 0;
    while((c_tmp = getch()) != 10){

        if((int)c_tmp == 127 || (int)c_tmp == 8 ){
            delch();
            delch();
            snprintf(buff,compte,"%s",buff);
            memset((buff+compte), '0', (50-compte)+1);
        }else{
         compte++;
         sprintf(buff,"%s%c",buff,c_tmp);
     }
     mvprintw(LINES-5, 4, buff); 
     mvprintw(2, 2,buff); 
 }
 return std::string(buff);

}
Touche captureTouche(){
//TODO
}

void dessinerTexteEtat(Coordonnee c,const std::string& tour){
    char efface[50];
    memset(efface,' ', 50);
    mvvline(LINES-5, COLS-50,' ', 50);
    mvprintw(LINES-5, COLS-50, efface); 
    mvprintw(LINES-5, COLS-50, tour.c_str()); 
}

void dessinerTexteDebug(Coordonnee c,const std::string& tour){
    char efface[50] = {0};
    memset(efface,' ', 50);
    mvvline(LINES-4, COLS-50,' ', 50);
    mvprintw(LINES-4, COLS-50, efface); 
    mvprintw(LINES-4, COLS-50, tour.c_str()); 
}
void dessinerListeBateau(Coordonnee c,std::vector<Bateau>* b){
   int compteur_y = 0;
   std::string message; 
   this->win_liste_bateau = newwin(b->size(), 40,c.getY()+5, c.getX()+20) ;
   int couleur = 0;
   mvwprintw(win_liste_bateau,compteur_y, 0,"liste des bateaux"); 
   compteur_y++;
   for (std::vector<Bateau>::iterator i = b->begin(); i != b->end(); ++i)
   {
    if((*i).estCoule()){
        couleur = CURSE_ROUGE;
    }else
    if(((*i).getCoordonnee().getX() == 0) && ((*i).getCoordonnee().getY() == 0)){
        couleur = CURSE_JAUNE;
    }else{
        couleur = CURSE_BLEU;
    }
    wattron (win_liste_bateau, COLOR_PAIR (couleur));
    message = (*i).getNom();
    mvwprintw(win_liste_bateau,compteur_y, 0, message.c_str()); 
    wattroff (win_liste_bateau,COLOR_PAIR(couleur));
    compteur_y++;

}

wrefresh(win_liste_bateau);
refresh();
}
void rafraichir(){
    wrefresh(this->grid);
    wrefresh(this->grid_adverse);
    
    refresh();

}
};



//


#endif