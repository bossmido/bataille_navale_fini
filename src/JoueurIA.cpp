
#include "JoueurIA.h"
#include "GrillePersonnel.h"

void JoueurIA::jouer( std::string& chaine, Joueur& j, Joueur& adversaire,const Option& o){
    int x = (rand() % j.GetGrillePersonnel().getLargeur());
    int y = (rand() % j.GetGrillePersonnel().getHauteur());
    Coordonnee coordonne_ou_c_qon_tire = Coordonnee(x,y);
    j.tirer(coordonne_ou_c_qon_tire,adversaire);
}