#include "Grille.h"

Grille::Grille(int hauteur,int largeur){
    this->type = AUCUN;
  this->largeur = largeur; 
  this->hauteur = hauteur;
  this->listes_cases =new std::vector< std::vector<Case> >(this->largeur, std::vector<Case>(this->hauteur));

}

Grille::Grille(int hauteur,int largeur,Type_grille t){
    this->type = AUCUN;
  this->largeur = largeur; 
  this->hauteur = hauteur;
  this->listes_cases =new std::vector< std::vector<Case> >(this->largeur, std::vector<Case>(this->hauteur));
  this->type = t;
}
Grille::Grille(){
  this->type = AUCUN;
  this->largeur = 10; 
  this->hauteur = 10;
  this->listes_cases =new std::vector< std::vector<Case> >(this->largeur, std::vector<Case>(this->hauteur));

}

Grille::Grille(Type_grille t){
  this->type = AUCUN;
  this->largeur = 10; 
  this->hauteur = 10;
  this->listes_cases =new std::vector< std::vector<Case> >(this->largeur, std::vector<Case>(this->hauteur));
  this->type = t;
}
void Grille::dessiner(Coordonnee base_adresse){
  canvas->dessinerGrille(base_adresse,*this);
}

Case Grille::getCase(Coordonnee c)
{
  return this->listes_cases->at(c.getX())[c.getY()];
}

std::vector<std::vector <Case> > & Grille::getCases()
{
  return *this->listes_cases;
}

int Grille::getLargeur() const
{
  return this->largeur;
}
void Grille::setLargeur(int arg)
{
  this->largeur = arg;
}


int Grille::getHauteur() const
{
  return this->hauteur;
}
void Grille::setHauteur(int arg)
{
  this->hauteur = arg;
}

void Grille::marquerCase(Coordonnee c, Etatgrille e)
{
  this->listes_cases->at(c.getX())[c.getY()] = e;
}
void Grille::setCoordonnee(Coordonnee arg) {
  this->coordonnee_dessin = arg;
}
Coordonnee Grille::getCoordonnee() const{
  return this->coordonnee_dessin;
}


Type_grille Grille::getType() const{
  return this->type;
}
void Grille::setType(Type_grille t){
  this->type = t;
}