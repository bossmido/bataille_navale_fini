#include "Coordonnee.h"
#include "utilitaire.h"
struct ConfigDessin{
    //partie dessin du menu
    int offset_y_pre_menu;
    int nb_entre;
    int largeur_max;
    Coordonnee* position_absolu_menu;
    //partie dessin de la premiere grille de possition des bateaux du joueur
    int offset_x_pre_grille;
    int offset_y_post_menu;
    int hauteur_grille;
    int largeur_grille;
    int hauteur_case;
    int largeur_case;
    Coordonnee* position_absolu_grille_joueur;
    Coordonnee* position_grille_joueur_reel;
    //partie dessin des grilles ennemis
    int separation_grille;
    Coordonnee* position_absolu_grille_adverse;
    Coordonnee* position_absolu_grille_adverse_reel;
    //partie desin commentaire (tour joueur X et et grille joueur Y)
    int offset_y_post_grille;
    Coordonnee* position_absolu_str_info_tour;
    Coordonnee* position_absolu_str_info_adverse;

    //partie dessin du compte a rebours
    int offset_y_pre_compte;
    Coordonnee* position_absolu_compte;

    void initialiserValeur(int hauteur_grille,int largeur_grille){
        offset_y_pre_menu = 1;
        nb_entre = 3;
        largeur_max = 10;
        offset_x_pre_grille = 2;
        offset_y_post_menu = 2;
        this->hauteur_grille = hauteur_grille;
        this->largeur_grille = largeur_grille;
        hauteur_case = 4;
        largeur_case =4;
        separation_grille = 5;
        offset_y_post_grille = 4;
        offset_y_pre_compte = 2;

    }
    void deduireCoordonnee(){
        position_absolu_menu =new  Coordonnee((largeur_terminal()/2)-(largeur_max/2),offset_y_post_menu);
        position_absolu_grille_joueur = new Coordonnee(offset_x_pre_grille,offset_y_post_menu+nb_entre+offset_x_pre_grille);
        position_grille_joueur_reel = new Coordonnee(1,1);
        *position_grille_joueur_reel = *position_grille_joueur_reel + *position_absolu_grille_joueur;
        position_absolu_grille_adverse =new  Coordonnee((*position_absolu_grille_joueur).getX()+1+largeur_case*(1+largeur_case) + separation_grille,(*position_absolu_grille_joueur).getY());
        position_absolu_grille_adverse_reel  =  new Coordonnee((*position_absolu_grille_adverse).getX()+1,(*position_absolu_grille_adverse).getY()+1);

        position_absolu_str_info_tour = new Coordonnee(position_absolu_grille_joueur->getX(),position_absolu_grille_joueur->getY() + 1+offset_y_post_grille+hauteur_case*(1+hauteur_case));
        position_absolu_str_info_adverse = new Coordonnee(position_absolu_grille_adverse->getX(),position_absolu_grille_joueur->getY() + 1+offset_y_post_grille +hauteur_case*(1+hauteur_case));




    }

    //config par defaut (centré sur l'affichage du mode texte)
    ConfigDessin(int hauteur_grille,int largeur_grille){
        this->initialiserValeur(hauteur_grille,largeur_grille);
        this->deduireCoordonnee();

    }

};