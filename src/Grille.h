#ifndef Grille_h
#define Grille_h
#include <vector>
#include <wchar.h>
#include "utilitaire.h"
#include "Case.h"
#include "Coordonnee.h"
class Dessinnable;
#include "Dessinnable.h"


extern Dessinnable * canvas;
enum Type_grille{JOUEUR=0,ADVERSAIRE=1,AUCUN=2};
class Grille {

public:    
    Grille(int hauteur,int largeur);
    Grille(int hauteur,int largeur,Type_grille t);
    Grille();
    Grille(Type_grille t);
    virtual Case getCase(Coordonnee c);
    virtual void dessiner(Coordonnee base_adresse);
    virtual  std::vector<std::vector <Case> > & getCases();
    virtual int getLargeur() const;
    virtual void setLargeur(int arg);
    virtual int getHauteur() const;
    virtual void setHauteur(int arg);
    virtual Coordonnee getCoordonnee() const;
    virtual void setCoordonnee(Coordonnee arg);
    virtual void marquerCase(Coordonnee c, Etatgrille e);
    virtual Type_grille getType() const;
    virtual void setType(Type_grille t);


    
protected:
    Coordonnee coordonnee_dessin;// besoin pour le desin ddu contenu d ela grille sinon dessine n'importe ou quoi...
    int largeur,hauteur;
    std::vector<std::vector <Case> >* listes_cases;
    Type_grille type;
};

#endif // Grille_h
